INSERT INTO public.kalendar(
datum, druh_dne, svatek, poznamka)
VALUES
(TO_DATE('1/1/2021', 'MM/DD/YYYY'),'S','Nový rok',' ')
,
(TO_DATE('1/2/2021', 'MM/DD/YYYY'),'V','Karina',' ')
,
(TO_DATE('1/3/2021', 'MM/DD/YYYY'),'V','Radmila',' ')
,
(TO_DATE('1/4/2021', 'MM/DD/YYYY'),'P','Diana',' ')
,
(TO_DATE('1/5/2021', 'MM/DD/YYYY'),'P','Dalimil',' ')
,
(TO_DATE('1/6/2021', 'MM/DD/YYYY'),'P','Tři králové',' ')
,
(TO_DATE('1/7/2021', 'MM/DD/YYYY'),'P','Vilma',' ')
,
(TO_DATE('1/8/2021', 'MM/DD/YYYY'),'P','Čestmír',' ')
,
(TO_DATE('1/9/2021', 'MM/DD/YYYY'),'V','Vladan',' ')
,
(TO_DATE('1/10/2021', 'MM/DD/YYYY'),'V','Břetislav',' ')
,
(TO_DATE('1/11/2021', 'MM/DD/YYYY'),'P','Bohdana',' ')
,
(TO_DATE('1/12/2021', 'MM/DD/YYYY'),'P','Pravoslav',' ')
,
(TO_DATE('1/13/2021', 'MM/DD/YYYY'),'P','Edita',' ')
,
(TO_DATE('1/14/2021', 'MM/DD/YYYY'),'P','Radovan',' ')
,
(TO_DATE('1/15/2021', 'MM/DD/YYYY'),'P','Alice',' ')
,
(TO_DATE('1/16/2021', 'MM/DD/YYYY'),'V','Ctirad',' ')
,
(TO_DATE('1/17/2021', 'MM/DD/YYYY'),'V','Drahoslav',' ')
,
(TO_DATE('1/18/2021', 'MM/DD/YYYY'),'P','Vladislav',' ')
,
(TO_DATE('1/19/2021', 'MM/DD/YYYY'),'P','Doubravka',' ')
,
(TO_DATE('1/20/2021', 'MM/DD/YYYY'),'P','Ilona',' ')
,
(TO_DATE('1/21/2021', 'MM/DD/YYYY'),'P','Běla',' ')
,
(TO_DATE('1/22/2021', 'MM/DD/YYYY'),'P','Slavomír',' ')
,
(TO_DATE('1/23/2021', 'MM/DD/YYYY'),'V','Zdeněk',' ')
,
(TO_DATE('1/24/2021', 'MM/DD/YYYY'),'V','Milena',' ')
,
(TO_DATE('1/25/2021', 'MM/DD/YYYY'),'P','Miloš',' ')
,
(TO_DATE('1/26/2021', 'MM/DD/YYYY'),'P','Zora',' ')
,
(TO_DATE('1/27/2021', 'MM/DD/YYYY'),'P','Ingrid',' ')
,
(TO_DATE('1/28/2021', 'MM/DD/YYYY'),'P','Otýlie',' ')
,
(TO_DATE('1/29/2021', 'MM/DD/YYYY'),'P','Zdislava',' ')
,
(TO_DATE('1/30/2021', 'MM/DD/YYYY'),'V','Robin',' ')
,
(TO_DATE('1/31/2021', 'MM/DD/YYYY'),'V','Marika',' ')
,
(TO_DATE('2/1/2021', 'MM/DD/YYYY'),'P','Hynek',' ')
,
(TO_DATE('2/2/2021', 'MM/DD/YYYY'),'P','Nela',' ')
,
(TO_DATE('2/3/2021', 'MM/DD/YYYY'),'P','Blažej',' ')
,
(TO_DATE('2/4/2021', 'MM/DD/YYYY'),'P','Jarmila',' ')
,
(TO_DATE('2/5/2021', 'MM/DD/YYYY'),'P','Dobromila',' ')
,
(TO_DATE('2/6/2021', 'MM/DD/YYYY'),'V','Vanda',' ')
,
(TO_DATE('2/7/2021', 'MM/DD/YYYY'),'V','Veronika',' ')
,
(TO_DATE('2/8/2021', 'MM/DD/YYYY'),'P','Milada',' ')
,
(TO_DATE('2/9/2021', 'MM/DD/YYYY'),'P','Apolena',' ')
,
(TO_DATE('2/10/2021', 'MM/DD/YYYY'),'P','Mojmír',' ')
,
(TO_DATE('2/11/2021', 'MM/DD/YYYY'),'P','Božena',' ')
,
(TO_DATE('2/12/2021', 'MM/DD/YYYY'),'P','Slavěna',' ')
,
(TO_DATE('2/13/2021', 'MM/DD/YYYY'),'V','Věnceslav',' ')
,
(TO_DATE('2/14/2021', 'MM/DD/YYYY'),'V','Valentýn',' ')
,
(TO_DATE('2/15/2021', 'MM/DD/YYYY'),'P','Jiřina',' ')
,
(TO_DATE('2/16/2021', 'MM/DD/YYYY'),'P','Ljuba',' ')
,
(TO_DATE('2/17/2021', 'MM/DD/YYYY'),'P','Miloslava',' ')
,
(TO_DATE('2/18/2021', 'MM/DD/YYYY'),'P','Gizela',' ')
,
(TO_DATE('2/19/2021', 'MM/DD/YYYY'),'P','Patrik',' ')
,
(TO_DATE('2/20/2021', 'MM/DD/YYYY'),'V','Oldřich',' ')
,
(TO_DATE('2/21/2021', 'MM/DD/YYYY'),'V','Lenka',' ')
,
(TO_DATE('2/22/2021', 'MM/DD/YYYY'),'P','Petr',' ')
,
(TO_DATE('2/23/2021', 'MM/DD/YYYY'),'P','Svatopluk',' ')
,
(TO_DATE('2/24/2021', 'MM/DD/YYYY'),'P','Matěj',' ')
,
(TO_DATE('2/25/2021', 'MM/DD/YYYY'),'P','Liliana',' ')
,
(TO_DATE('2/26/2021', 'MM/DD/YYYY'),'P','Dorota',' ')
,
(TO_DATE('2/27/2021', 'MM/DD/YYYY'),'V','Alexandr',' ')
,
(TO_DATE('2/28/2021', 'MM/DD/YYYY'),'V','Lumír',' ')
,
(TO_DATE('3/1/2021', 'MM/DD/YYYY'),'P','Bedřich',' ')
,
(TO_DATE('3/2/2021', 'MM/DD/YYYY'),'P','Anežka',' ')
,
(TO_DATE('3/3/2021', 'MM/DD/YYYY'),'P','Kamil',' ')
,
(TO_DATE('3/4/2021', 'MM/DD/YYYY'),'P','Stela',' ')
,
(TO_DATE('3/5/2021', 'MM/DD/YYYY'),'P','Kazimír',' ')
,
(TO_DATE('3/6/2021', 'MM/DD/YYYY'),'V','Miroslav',' ')
,
(TO_DATE('3/7/2021', 'MM/DD/YYYY'),'V','Tomáš',' ')
,
(TO_DATE('3/8/2021', 'MM/DD/YYYY'),'P','Gabriela',' ')
,
(TO_DATE('3/9/2021', 'MM/DD/YYYY'),'P','Františka',' ')
,
(TO_DATE('3/10/2021', 'MM/DD/YYYY'),'P','Viktorie',' ')
,
(TO_DATE('3/11/2021', 'MM/DD/YYYY'),'P','Anděla',' ')
,
(TO_DATE('3/12/2021', 'MM/DD/YYYY'),'P','Řehoř',' ')
,
(TO_DATE('3/13/2021', 'MM/DD/YYYY'),'V','Růžena',' ')
,
(TO_DATE('3/14/2021', 'MM/DD/YYYY'),'V','Rút',' ')
,
(TO_DATE('3/15/2021', 'MM/DD/YYYY'),'P','Ida',' ')
,
(TO_DATE('3/16/2021', 'MM/DD/YYYY'),'P','Elena',' ')
,
(TO_DATE('3/17/2021', 'MM/DD/YYYY'),'P','Vlastimil',' ')
,
(TO_DATE('3/18/2021', 'MM/DD/YYYY'),'P','Eduard',' ')
,
(TO_DATE('3/19/2021', 'MM/DD/YYYY'),'P','Josef',' ')
,
(TO_DATE('3/20/2021', 'MM/DD/YYYY'),'V','Světlana',' ')
,
(TO_DATE('3/21/2021', 'MM/DD/YYYY'),'V','Radek',' ')
,
(TO_DATE('3/22/2021', 'MM/DD/YYYY'),'P','Leona',' ')
,
(TO_DATE('3/23/2021', 'MM/DD/YYYY'),'P','Ivona',' ')
,
(TO_DATE('3/24/2021', 'MM/DD/YYYY'),'P','Gabriel',' ')
,
(TO_DATE('3/25/2021', 'MM/DD/YYYY'),'P','Marián',' ')
,
(TO_DATE('3/26/2021', 'MM/DD/YYYY'),'P','Emanuel',' ')
,
(TO_DATE('3/27/2021', 'MM/DD/YYYY'),'V','Dita',' ')
,
(TO_DATE('3/28/2021', 'MM/DD/YYYY'),'V','Soňa',' ')
,
(TO_DATE('3/29/2021', 'MM/DD/YYYY'),'P','Taťána',' ')
,
(TO_DATE('3/30/2021', 'MM/DD/YYYY'),'P','Arnošt',' ')
,
(TO_DATE('3/31/2021', 'MM/DD/YYYY'),'P','Kvido',' ')
,
(TO_DATE('4/1/2021', 'MM/DD/YYYY'),'P','Hugo',' ')
,
(TO_DATE('4/2/2021', 'MM/DD/YYYY'),'S','Erika',' ')
,
(TO_DATE('4/3/2021', 'MM/DD/YYYY'),'V','Richard',' ')
,
(TO_DATE('4/4/2021', 'MM/DD/YYYY'),'V','Ivana',' ')
,
(TO_DATE('4/5/2021', 'MM/DD/YYYY'),'S','Miroslava',' ')
,
(TO_DATE('4/6/2021', 'MM/DD/YYYY'),'P','Vendula',' ')
,
(TO_DATE('4/7/2021', 'MM/DD/YYYY'),'P','Heřman',' ')
,
(TO_DATE('4/8/2021', 'MM/DD/YYYY'),'P','Ema',' ')
,
(TO_DATE('4/9/2021', 'MM/DD/YYYY'),'P','Dušan',' ')
,
(TO_DATE('4/10/2021', 'MM/DD/YYYY'),'V','Darja',' ')
,
(TO_DATE('4/11/2021', 'MM/DD/YYYY'),'V','Izabela',' ')
,
(TO_DATE('4/12/2021', 'MM/DD/YYYY'),'P','Julius',' ')
,
(TO_DATE('4/13/2021', 'MM/DD/YYYY'),'P','Aleš',' ')
,
(TO_DATE('4/14/2021', 'MM/DD/YYYY'),'P','Vincenc',' ')
,
(TO_DATE('4/15/2021', 'MM/DD/YYYY'),'P','Anastázie',' ')
,
(TO_DATE('4/16/2021', 'MM/DD/YYYY'),'P','Irena',' ')
,
(TO_DATE('4/17/2021', 'MM/DD/YYYY'),'V','Rudolf',' ')
,
(TO_DATE('4/18/2021', 'MM/DD/YYYY'),'V','Valérie',' ')
,
(TO_DATE('4/19/2021', 'MM/DD/YYYY'),'P','Rostislav',' ')
,
(TO_DATE('4/20/2021', 'MM/DD/YYYY'),'P','Marcela',' ')
,
(TO_DATE('4/21/2021', 'MM/DD/YYYY'),'P','Alexandra',' ')
,
(TO_DATE('4/22/2021', 'MM/DD/YYYY'),'P','Evženie',' ')
,
(TO_DATE('4/23/2021', 'MM/DD/YYYY'),'P','Vojtěch',' ')
,
(TO_DATE('4/24/2021', 'MM/DD/YYYY'),'V','Jiří',' ')
,
(TO_DATE('4/25/2021', 'MM/DD/YYYY'),'V','Marek',' ')
,
(TO_DATE('4/26/2021', 'MM/DD/YYYY'),'P','Oto',' ')
,
(TO_DATE('4/27/2021', 'MM/DD/YYYY'),'P','Jaroslav',' ')
,
(TO_DATE('4/28/2021', 'MM/DD/YYYY'),'P','Vlastislav',' ')
,
(TO_DATE('4/29/2021', 'MM/DD/YYYY'),'P','Robert',' ')
,
(TO_DATE('4/30/2021', 'MM/DD/YYYY'),'P','Blahoslav',' ')
,
(TO_DATE('5/1/2021', 'MM/DD/YYYY'),'S','Svátek práce',' ')
,
(TO_DATE('5/2/2021', 'MM/DD/YYYY'),'V','Zikmund',' ')
,
(TO_DATE('5/3/2021', 'MM/DD/YYYY'),'P','Alexej',' ')
,
(TO_DATE('5/4/2021', 'MM/DD/YYYY'),'P','Květoslav',' ')
,
(TO_DATE('5/5/2021', 'MM/DD/YYYY'),'P','Klaudie',' ')
,
(TO_DATE('5/6/2021', 'MM/DD/YYYY'),'P','Radoslav',' ')
,
(TO_DATE('5/7/2021', 'MM/DD/YYYY'),'P','Stanislav',' ')
,
(TO_DATE('5/8/2021', 'MM/DD/YYYY'),'S','Státní svátek',' ')
,
(TO_DATE('5/9/2021', 'MM/DD/YYYY'),'V','Ctibor',' ')
,
(TO_DATE('5/10/2021', 'MM/DD/YYYY'),'P','Blažena',' ')
,
(TO_DATE('5/11/2021', 'MM/DD/YYYY'),'P','Svatava',' ')
,
(TO_DATE('5/12/2021', 'MM/DD/YYYY'),'P','Pankrác',' ')
,
(TO_DATE('5/13/2021', 'MM/DD/YYYY'),'P','Servác',' ')
,
(TO_DATE('5/14/2021', 'MM/DD/YYYY'),'P','Bonifác',' ')
,
(TO_DATE('5/15/2021', 'MM/DD/YYYY'),'V','Žofie',' ')
,
(TO_DATE('5/16/2021', 'MM/DD/YYYY'),'V','Přemysl',' ')
,
(TO_DATE('5/17/2021', 'MM/DD/YYYY'),'P','Aneta',' ')
,
(TO_DATE('5/18/2021', 'MM/DD/YYYY'),'P','Nataša',' ')
,
(TO_DATE('5/19/2021', 'MM/DD/YYYY'),'P','Ivo',' ')
,
(TO_DATE('5/20/2021', 'MM/DD/YYYY'),'P','Zbyšek',' ')
,
(TO_DATE('5/21/2021', 'MM/DD/YYYY'),'P','Monika',' ')
,
(TO_DATE('5/22/2021', 'MM/DD/YYYY'),'V','Emil',' ')
,
(TO_DATE('5/23/2021', 'MM/DD/YYYY'),'V','Vladimír',' ')
,
(TO_DATE('5/24/2021', 'MM/DD/YYYY'),'P','Jana',' ')
,
(TO_DATE('5/25/2021', 'MM/DD/YYYY'),'P','Viola',' ')
,
(TO_DATE('5/26/2021', 'MM/DD/YYYY'),'P','Filip',' ')
,
(TO_DATE('5/27/2021', 'MM/DD/YYYY'),'P','Valdemar',' ')
,
(TO_DATE('5/28/2021', 'MM/DD/YYYY'),'P','Vilém',' ')
,
(TO_DATE('5/29/2021', 'MM/DD/YYYY'),'V','Maxmilián',' ')
,
(TO_DATE('5/30/2021', 'MM/DD/YYYY'),'V','Ferdinand',' ')
,
(TO_DATE('5/31/2021', 'MM/DD/YYYY'),'P','Kamila',' ')
,
(TO_DATE('6/1/2021', 'MM/DD/YYYY'),'P','Laura',' ')
,
(TO_DATE('6/2/2021', 'MM/DD/YYYY'),'P','Jarmil',' ')
,
(TO_DATE('6/3/2021', 'MM/DD/YYYY'),'P','Tamara',' ')
,
(TO_DATE('6/4/2021', 'MM/DD/YYYY'),'P','Dalibor',' ')
,
(TO_DATE('6/5/2021', 'MM/DD/YYYY'),'V','Dobroslav',' ')
,
(TO_DATE('6/6/2021', 'MM/DD/YYYY'),'V','Norbert',' ')
,
(TO_DATE('6/7/2021', 'MM/DD/YYYY'),'P','Iveta',' ')
,
(TO_DATE('6/8/2021', 'MM/DD/YYYY'),'P','Medard',' ')
,
(TO_DATE('6/9/2021', 'MM/DD/YYYY'),'P','Stanislava',' ')
,
(TO_DATE('6/10/2021', 'MM/DD/YYYY'),'P','Gita',' ')
,
(TO_DATE('6/11/2021', 'MM/DD/YYYY'),'P','Bruno',' ')
,
(TO_DATE('6/12/2021', 'MM/DD/YYYY'),'V','Antonie',' ')
,
(TO_DATE('6/13/2021', 'MM/DD/YYYY'),'V','Antonín',' ')
,
(TO_DATE('6/14/2021', 'MM/DD/YYYY'),'P','Roland',' ')
,
(TO_DATE('6/15/2021', 'MM/DD/YYYY'),'P','Vít',' ')
,
(TO_DATE('6/16/2021', 'MM/DD/YYYY'),'P','Zbyněk',' ')
,
(TO_DATE('6/17/2021', 'MM/DD/YYYY'),'P','Adolf',' ')
,
(TO_DATE('6/18/2021', 'MM/DD/YYYY'),'P','Milan',' ')
,
(TO_DATE('6/19/2021', 'MM/DD/YYYY'),'V','Leoš',' ')
,
(TO_DATE('6/20/2021', 'MM/DD/YYYY'),'V','Květa',' ')
,
(TO_DATE('6/21/2021', 'MM/DD/YYYY'),'P','Alois',' ')
,
(TO_DATE('6/22/2021', 'MM/DD/YYYY'),'P','Pavla',' ')
,
(TO_DATE('6/23/2021', 'MM/DD/YYYY'),'P','Zdeňka',' ')
,
(TO_DATE('6/24/2021', 'MM/DD/YYYY'),'P','Jan',' ')
,
(TO_DATE('6/25/2021', 'MM/DD/YYYY'),'P','Ivan',' ')
,
(TO_DATE('6/26/2021', 'MM/DD/YYYY'),'V','Adriana',' ')
,
(TO_DATE('6/27/2021', 'MM/DD/YYYY'),'V','Ladislav',' ')
,
(TO_DATE('6/28/2021', 'MM/DD/YYYY'),'P','Lubomír',' ')
,
(TO_DATE('6/29/2021', 'MM/DD/YYYY'),'P','Petr a Pavel',' ')
,
(TO_DATE('6/30/2021', 'MM/DD/YYYY'),'P','Šárka',' ')
,
(TO_DATE('7/1/2021', 'MM/DD/YYYY'),'P','Jaroslava',' ')
,
(TO_DATE('7/2/2021', 'MM/DD/YYYY'),'P','Patricie',' ')
,
(TO_DATE('7/3/2021', 'MM/DD/YYYY'),'V','Radomír',' ')
,
(TO_DATE('7/4/2021', 'MM/DD/YYYY'),'V','Prokop',' ')
,
(TO_DATE('7/5/2021', 'MM/DD/YYYY'),'S','Cyril a Metoděj',' ')
,
(TO_DATE('7/6/2021', 'MM/DD/YYYY'),'S','mistr Jan Hus',' ')
,
(TO_DATE('7/7/2021', 'MM/DD/YYYY'),'P','Bohuslava',' ')
,
(TO_DATE('7/8/2021', 'MM/DD/YYYY'),'P','Nora',' ')
,
(TO_DATE('7/9/2021', 'MM/DD/YYYY'),'P','Drahoslava',' ')
,
(TO_DATE('7/10/2021', 'MM/DD/YYYY'),'V','Libuše',' ')
,
(TO_DATE('7/11/2021', 'MM/DD/YYYY'),'V','Olga',' ')
,
(TO_DATE('7/12/2021', 'MM/DD/YYYY'),'P','Borek',' ')
,
(TO_DATE('7/13/2021', 'MM/DD/YYYY'),'P','Markéta',' ')
,
(TO_DATE('7/14/2021', 'MM/DD/YYYY'),'P','Karolína',' ')
,
(TO_DATE('7/15/2021', 'MM/DD/YYYY'),'P','Jindřich',' ')
,
(TO_DATE('7/16/2021', 'MM/DD/YYYY'),'P','Luboš',' ')
,
(TO_DATE('7/17/2021', 'MM/DD/YYYY'),'V','Martina',' ')
,
(TO_DATE('7/18/2021', 'MM/DD/YYYY'),'V','Drahomíra',' ')
,
(TO_DATE('7/19/2021', 'MM/DD/YYYY'),'P','Čeněk',' ')
,
(TO_DATE('7/20/2021', 'MM/DD/YYYY'),'P','Ilja',' ')
,
(TO_DATE('7/21/2021', 'MM/DD/YYYY'),'P','Vítězslav',' ')
,
(TO_DATE('7/22/2021', 'MM/DD/YYYY'),'P','Magdaléna',' ')
,
(TO_DATE('7/23/2021', 'MM/DD/YYYY'),'P','Libor',' ')
,
(TO_DATE('7/24/2021', 'MM/DD/YYYY'),'V','Kristýna',' ')
,
(TO_DATE('7/25/2021', 'MM/DD/YYYY'),'V','Jakub',' ')
,
(TO_DATE('7/26/2021', 'MM/DD/YYYY'),'P','Anna',' ')
,
(TO_DATE('7/27/2021', 'MM/DD/YYYY'),'P','Věroslav',' ')
,
(TO_DATE('7/28/2021', 'MM/DD/YYYY'),'P','Viktor',' ')
,
(TO_DATE('7/29/2021', 'MM/DD/YYYY'),'P','Marta',' ')
,
(TO_DATE('7/30/2021', 'MM/DD/YYYY'),'P','Bořivoj',' ')
,
(TO_DATE('7/31/2021', 'MM/DD/YYYY'),'V','Ignác',' ')
,
(TO_DATE('8/1/2021', 'MM/DD/YYYY'),'V','Oskar',' ')
,
(TO_DATE('8/2/2021', 'MM/DD/YYYY'),'P','Gustav',' ')
,
(TO_DATE('8/3/2021', 'MM/DD/YYYY'),'P','Miluše',' ')
,
(TO_DATE('8/4/2021', 'MM/DD/YYYY'),'P','Dominik',' ')
,
(TO_DATE('8/5/2021', 'MM/DD/YYYY'),'P','Kristián',' ')
,
(TO_DATE('8/6/2021', 'MM/DD/YYYY'),'P','Oldřiška',' ')
,
(TO_DATE('8/7/2021', 'MM/DD/YYYY'),'V','Lada',' ')
,
(TO_DATE('8/8/2021', 'MM/DD/YYYY'),'V','Soběslav',' ')
,
(TO_DATE('8/9/2021', 'MM/DD/YYYY'),'P','Roman',' ')
,
(TO_DATE('8/10/2021', 'MM/DD/YYYY'),'P','Vavřinec',' ')
,
(TO_DATE('8/11/2021', 'MM/DD/YYYY'),'P','Zuzana',' ')
,
(TO_DATE('8/12/2021', 'MM/DD/YYYY'),'P','Klára',' ')
,
(TO_DATE('8/13/2021', 'MM/DD/YYYY'),'P','Alena',' ')
,
(TO_DATE('8/14/2021', 'MM/DD/YYYY'),'V','Alan',' ')
,
(TO_DATE('8/15/2021', 'MM/DD/YYYY'),'V','Hana',' ')
,
(TO_DATE('8/16/2021', 'MM/DD/YYYY'),'P','Jáchym',' ')
,
(TO_DATE('8/17/2021', 'MM/DD/YYYY'),'P','Petra',' ')
,
(TO_DATE('8/18/2021', 'MM/DD/YYYY'),'P','Helena',' ')
,
(TO_DATE('8/19/2021', 'MM/DD/YYYY'),'P','Ludvík',' ')
,
(TO_DATE('8/20/2021', 'MM/DD/YYYY'),'P','Bernard',' ')
,
(TO_DATE('8/21/2021', 'MM/DD/YYYY'),'V','Johana',' ')
,
(TO_DATE('8/22/2021', 'MM/DD/YYYY'),'V','Bohuslav',' ')
,
(TO_DATE('8/23/2021', 'MM/DD/YYYY'),'P','Sandra',' ')
,
(TO_DATE('8/24/2021', 'MM/DD/YYYY'),'P','Bartoloměj',' ')
,
(TO_DATE('8/25/2021', 'MM/DD/YYYY'),'P','Radim',' ')
,
(TO_DATE('8/26/2021', 'MM/DD/YYYY'),'P','Luděk',' ')
,
(TO_DATE('8/27/2021', 'MM/DD/YYYY'),'P','Otakar',' ')
,
(TO_DATE('8/28/2021', 'MM/DD/YYYY'),'V','Augustýn',' ')
,
(TO_DATE('8/29/2021', 'MM/DD/YYYY'),'V','Evelína',' ')
,
(TO_DATE('8/30/2021', 'MM/DD/YYYY'),'P','Vladěna',' ')
,
(TO_DATE('8/31/2021', 'MM/DD/YYYY'),'P','Pavlína',' ')
,
(TO_DATE('9/1/2021', 'MM/DD/YYYY'),'P','Linda',' ')
,
(TO_DATE('9/2/2021', 'MM/DD/YYYY'),'P','Adéla',' ')
,
(TO_DATE('9/3/2021', 'MM/DD/YYYY'),'P','Bronislav',' ')
,
(TO_DATE('9/4/2021', 'MM/DD/YYYY'),'V','Jindřiška',' ')
,
(TO_DATE('9/5/2021', 'MM/DD/YYYY'),'V','Boris',' ')
,
(TO_DATE('9/6/2021', 'MM/DD/YYYY'),'P','Boleslav',' ')
,
(TO_DATE('9/7/2021', 'MM/DD/YYYY'),'P','Regina',' ')
,
(TO_DATE('9/8/2021', 'MM/DD/YYYY'),'P','Mariana',' ')
,
(TO_DATE('9/9/2021', 'MM/DD/YYYY'),'P','Daniela',' ')
,
(TO_DATE('9/10/2021', 'MM/DD/YYYY'),'P','Irma',' ')
,
(TO_DATE('9/11/2021', 'MM/DD/YYYY'),'V','Denisa',' ')
,
(TO_DATE('9/12/2021', 'MM/DD/YYYY'),'V','Marie',' ')
,
(TO_DATE('9/13/2021', 'MM/DD/YYYY'),'P','Lubor',' ')
,
(TO_DATE('9/14/2021', 'MM/DD/YYYY'),'P','Radka',' ')
,
(TO_DATE('9/15/2021', 'MM/DD/YYYY'),'P','Jolana',' ')
,
(TO_DATE('9/16/2021', 'MM/DD/YYYY'),'P','Ludmila',' ')
,
(TO_DATE('9/17/2021', 'MM/DD/YYYY'),'P','Naděžda',' ')
,
(TO_DATE('9/18/2021', 'MM/DD/YYYY'),'V','Kryštof',' ')
,
(TO_DATE('9/19/2021', 'MM/DD/YYYY'),'V','Zita',' ')
,
(TO_DATE('9/20/2021', 'MM/DD/YYYY'),'P','Oleg',' ')
,
(TO_DATE('9/21/2021', 'MM/DD/YYYY'),'P','Matouš',' ')
,
(TO_DATE('9/22/2021', 'MM/DD/YYYY'),'P','Darina',' ')
,
(TO_DATE('9/23/2021', 'MM/DD/YYYY'),'P','Berta',' ')
,
(TO_DATE('9/24/2021', 'MM/DD/YYYY'),'P','Jaromír',' ')
,
(TO_DATE('9/25/2021', 'MM/DD/YYYY'),'V','Zlata',' ')
,
(TO_DATE('9/26/2021', 'MM/DD/YYYY'),'V','Andrea',' ')
,
(TO_DATE('9/27/2021', 'MM/DD/YYYY'),'P','Jonáš',' ')
,
(TO_DATE('9/28/2021', 'MM/DD/YYYY'),'S','Václav',' ')
,
(TO_DATE('9/29/2021', 'MM/DD/YYYY'),'P','Michal',' ')
,
(TO_DATE('9/30/2021', 'MM/DD/YYYY'),'P','Jeroným',' ')
,
(TO_DATE('10/1/2021', 'MM/DD/YYYY'),'P','Igor',' ')
,
(TO_DATE('10/2/2021', 'MM/DD/YYYY'),'V','Olívie/Oliver',' ')
,
(TO_DATE('10/3/2021', 'MM/DD/YYYY'),'V','Bohumil',' ')
,
(TO_DATE('10/4/2021', 'MM/DD/YYYY'),'P','František',' ')
,
(TO_DATE('10/5/2021', 'MM/DD/YYYY'),'P','Eliška',' ')
,
(TO_DATE('10/6/2021', 'MM/DD/YYYY'),'P','Hanuš',' ')
,
(TO_DATE('10/7/2021', 'MM/DD/YYYY'),'P','Justýna',' ')
,
(TO_DATE('10/8/2021', 'MM/DD/YYYY'),'P','Věra',' ')
,
(TO_DATE('10/9/2021', 'MM/DD/YYYY'),'V','Štefan',' ')
,
(TO_DATE('10/10/2021', 'MM/DD/YYYY'),'V','Marina',' ')
,
(TO_DATE('10/11/2021', 'MM/DD/YYYY'),'P','Andrej',' ')
,
(TO_DATE('10/12/2021', 'MM/DD/YYYY'),'P','Marcel',' ')
,
(TO_DATE('10/13/2021', 'MM/DD/YYYY'),'P','Renáta',' ')
,
(TO_DATE('10/14/2021', 'MM/DD/YYYY'),'P','Agáta',' ')
,
(TO_DATE('10/15/2021', 'MM/DD/YYYY'),'P','Tereza',' ')
,
(TO_DATE('10/16/2021', 'MM/DD/YYYY'),'V','Havel',' ')
,
(TO_DATE('10/17/2021', 'MM/DD/YYYY'),'V','Hedvika',' ')
,
(TO_DATE('10/18/2021', 'MM/DD/YYYY'),'P','Lukáš',' ')
,
(TO_DATE('10/19/2021', 'MM/DD/YYYY'),'P','Michaela',' ')
,
(TO_DATE('10/20/2021', 'MM/DD/YYYY'),'P','Vendelín',' ')
,
(TO_DATE('10/21/2021', 'MM/DD/YYYY'),'P','Brigita',' ')
,
(TO_DATE('10/22/2021', 'MM/DD/YYYY'),'P','Sabina',' ')
,
(TO_DATE('10/23/2021', 'MM/DD/YYYY'),'V','Teodor',' ')
,
(TO_DATE('10/24/2021', 'MM/DD/YYYY'),'V','Nina',' ')
,
(TO_DATE('10/25/2021', 'MM/DD/YYYY'),'P','Beáta',' ')
,
(TO_DATE('10/26/2021', 'MM/DD/YYYY'),'P','Erik',' ')
,
(TO_DATE('10/27/2021', 'MM/DD/YYYY'),'P','Šarlota',' ')
,
(TO_DATE('10/28/2021', 'MM/DD/YYYY'),'S','Státní svátek',' ')
,
(TO_DATE('10/29/2021', 'MM/DD/YYYY'),'P','Silvie',' ')
,
(TO_DATE('10/30/2021', 'MM/DD/YYYY'),'V','Tadeáš',' ')
,
(TO_DATE('10/31/2021', 'MM/DD/YYYY'),'V','Štěpánka',' ')
,
(TO_DATE('11/1/2021', 'MM/DD/YYYY'),'P','Felix',' ')
,
(TO_DATE('11/2/2021', 'MM/DD/YYYY'),'P','Pam.zesnulých',' ')
,
(TO_DATE('11/3/2021', 'MM/DD/YYYY'),'P','Hubert',' ')
,
(TO_DATE('11/4/2021', 'MM/DD/YYYY'),'P','Karel',' ')
,
(TO_DATE('11/5/2021', 'MM/DD/YYYY'),'P','Miriam',' ')
,
(TO_DATE('11/6/2021', 'MM/DD/YYYY'),'V','Liběna',' ')
,
(TO_DATE('11/7/2021', 'MM/DD/YYYY'),'V','Saskie',' ')
,
(TO_DATE('11/8/2021', 'MM/DD/YYYY'),'P','Bohumír',' ')
,
(TO_DATE('11/9/2021', 'MM/DD/YYYY'),'P','Bohdan',' ')
,
(TO_DATE('11/10/2021', 'MM/DD/YYYY'),'P','Evžen',' ')
,
(TO_DATE('11/11/2021', 'MM/DD/YYYY'),'P','Martin',' ')
,
(TO_DATE('11/12/2021', 'MM/DD/YYYY'),'P','Benedikt',' ')
,
(TO_DATE('11/13/2021', 'MM/DD/YYYY'),'V','Tibor',' ')
,
(TO_DATE('11/14/2021', 'MM/DD/YYYY'),'V','Sáva',' ')
,
(TO_DATE('11/15/2021', 'MM/DD/YYYY'),'P','Leopold',' ')
,
(TO_DATE('11/16/2021', 'MM/DD/YYYY'),'P','Otmar',' ')
,
(TO_DATE('11/17/2021', 'MM/DD/YYYY'),'S','Mahulena',' ')
,
(TO_DATE('11/18/2021', 'MM/DD/YYYY'),'P','Romana',' ')
,
(TO_DATE('11/19/2021', 'MM/DD/YYYY'),'P','Alžběta',' ')
,
(TO_DATE('11/20/2021', 'MM/DD/YYYY'),'V','Nikola',' ')
,
(TO_DATE('11/21/2021', 'MM/DD/YYYY'),'V','Albert',' ')
,
(TO_DATE('11/22/2021', 'MM/DD/YYYY'),'P','Cecílie',' ')
,
(TO_DATE('11/23/2021', 'MM/DD/YYYY'),'P','Klement',' ')
,
(TO_DATE('11/24/2021', 'MM/DD/YYYY'),'P','Emílie',' ')
,
(TO_DATE('11/25/2021', 'MM/DD/YYYY'),'P','Kateřina',' ')
,
(TO_DATE('11/26/2021', 'MM/DD/YYYY'),'P','Artur',' ')
,
(TO_DATE('11/27/2021', 'MM/DD/YYYY'),'V','Xenie',' ')
,
(TO_DATE('11/28/2021', 'MM/DD/YYYY'),'V','René',' ')
,
(TO_DATE('11/29/2021', 'MM/DD/YYYY'),'P','Zina',' ')
,
(TO_DATE('11/30/2021', 'MM/DD/YYYY'),'P','Ondřej',' ')
,
(TO_DATE('12/1/2021', 'MM/DD/YYYY'),'P','Iva',' ')
,
(TO_DATE('12/2/2021', 'MM/DD/YYYY'),'P','Blanka',' ')
,
(TO_DATE('12/3/2021', 'MM/DD/YYYY'),'P','Svatoslav',' ')
,
(TO_DATE('12/4/2021', 'MM/DD/YYYY'),'V','Barbora',' ')
,
(TO_DATE('12/5/2021', 'MM/DD/YYYY'),'V','Jitka',' ')
,
(TO_DATE('12/6/2021', 'MM/DD/YYYY'),'P','Mikuláš',' ')
,
(TO_DATE('12/7/2021', 'MM/DD/YYYY'),'P','Ambrož','Benjamín')
,
(TO_DATE('12/8/2021', 'MM/DD/YYYY'),'P','Květoslava',' ')
,
(TO_DATE('12/9/2021', 'MM/DD/YYYY'),'P','Vratislav',' ')
,
(TO_DATE('12/10/2021', 'MM/DD/YYYY'),'P','Julie',' ')
,
(TO_DATE('12/11/2021', 'MM/DD/YYYY'),'V','Dana',' ')
,
(TO_DATE('12/12/2021', 'MM/DD/YYYY'),'V','Simona',' ')
,
(TO_DATE('12/13/2021', 'MM/DD/YYYY'),'P','Lucie',' ')
,
(TO_DATE('12/14/2021', 'MM/DD/YYYY'),'P','Lýdie',' ')
,
(TO_DATE('12/15/2021', 'MM/DD/YYYY'),'P','Radana',' ')
,
(TO_DATE('12/16/2021', 'MM/DD/YYYY'),'P','Albína',' ')
,
(TO_DATE('12/17/2021', 'MM/DD/YYYY'),'P','Daniel',' ')
,
(TO_DATE('12/18/2021', 'MM/DD/YYYY'),'V','Miloslav',' ')
,
(TO_DATE('12/19/2021', 'MM/DD/YYYY'),'V','Ester',' ')
,
(TO_DATE('12/20/2021', 'MM/DD/YYYY'),'P','Dagmar',' ')
,
(TO_DATE('12/21/2021', 'MM/DD/YYYY'),'P','Natálie',' ')
,
(TO_DATE('12/22/2021', 'MM/DD/YYYY'),'P','Šimon',' ')
,
(TO_DATE('12/23/2021', 'MM/DD/YYYY'),'P','Vlasta',' ')
,
(TO_DATE('12/24/2021', 'MM/DD/YYYY'),'S','Adam a Eva',' ')
,
(TO_DATE('12/25/2021', 'MM/DD/YYYY'),'S','Boží hod',' ')
,
(TO_DATE('12/26/2021', 'MM/DD/YYYY'),'S','Štěpán',' ')
,
(TO_DATE('12/27/2021', 'MM/DD/YYYY'),'P','Žaneta',' ')
,
(TO_DATE('12/28/2021', 'MM/DD/YYYY'),'P','Bohumila',' ')
,
(TO_DATE('12/29/2021', 'MM/DD/YYYY'),'P','Judita',' ')
,
(TO_DATE('12/30/2021', 'MM/DD/YYYY'),'P','David',' ')
,
(TO_DATE('12/31/2021', 'MM/DD/YYYY'),'P','Silvestr',' ')
;
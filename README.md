# SAR-project



## Spuštění aplikace

### Spuštění serveru
Pro spuštění serveru stačí v application.properties nastavit připojení do
databáze a poté spustit třídu ServerApplication jako Spring Boot aplikaci.

Při prvním spuštění dojde k vytvoření databáze a deaultního uživatele (login: 1, heslo: admin).
Po vytvoření databáze je v ní nutné vytvořit základního zaměstnance pomocí skriptu zamestnanci.sql a
dále také přidat záznamy kalendáře pomocí kalendar.sql .

### Spuštění klienta
Klient se spouští ve složce client příkazy `npm i` a `npm start` .
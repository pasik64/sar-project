package com.sar.server.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class TimeUtils {

    public static String getTimeFromMinutes(int minutesFromMidnight) {
        String time = "";
        int hours = minutesFromMidnight / 60;
        int minutes = minutesFromMidnight % 60;
        String hoursString = hours < 10 ? "0" + hours : hours + "";
        String minutesString = minutes < 10? "0" + minutes : minutes + "";
        return hoursString + ":" + minutesString;
    }

    public static Date getMidnightDate(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        try {
            return formatter.parse(formatter.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date getCurrentDateWithoutMinutes() {
        Instant dayInst = LocalDate.now().atTime(0, 0).toLocalDate().atStartOfDay(ZoneId.systemDefault()).toInstant();
        return Date.from(dayInst);
    }

}

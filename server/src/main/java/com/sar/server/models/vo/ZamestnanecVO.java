package com.sar.server.models.vo;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@Table(	name = "zamestnanci",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "icp")
        })

public class ZamestnanecVO {

    @Id
    @Column(name = "icp", nullable = false, unique = true)
    private String icp;

    @NotBlank
    @Column(name = "jmeno", nullable = false)
    private String jmeno;

    @NotBlank
    @Column(name = "rod_c", nullable = false)
    private String rodC;

    @NotBlank
    @Column(name = "pomer_od", nullable = false)
    private Date pomerOd;

    @Column(name = "pomer_do")
    private Date pomerDo;

    @NotBlank
    @Column(name = "stred", nullable = false)
    private String stred; //stredisko

    @Column(name = "icp_ved")
    private String icpVed; //identifikacni cislo vedouciho

    public String getIcp() {
        return icp;
    }

    public void setIcp(String icp) {
        this.icp = icp;
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public String getRodC() {
        return rodC;
    }

    public void setRodC(String rodC) {
        this.rodC = rodC;
    }

    public Date getPomerOd() {
        return pomerOd;
    }

    public void setPomerOd(Date pomerOd) {
        this.pomerOd = pomerOd;
    }

    public Date getPomerDo() {
        return pomerDo;
    }

    public void setPomerDo(Date pomerDo) {
        this.pomerDo = pomerDo;
    }

    public String getStred() {
        return stred;
    }

    public void setStred(String stred) {
        this.stred = stred;
    }

    public String getIcpVed() {
        return icpVed;
    }

    public void setIcpVed(String icpVed) {
        this.icpVed = icpVed;
    }
}

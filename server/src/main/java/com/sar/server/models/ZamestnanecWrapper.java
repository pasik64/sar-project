package com.sar.server.models;

import java.util.Date;

public class ZamestnanecWrapper {
    private String icp;

    private String jmeno;

    private String rodC;

    private Date pomerOd;

    private Date pomerDo;

    private String stred;

    private String vedIcp;

    private String vedJmeno;

    public String getIcp() {
        return icp;
    }

    public void setIcp(String icp) {
        this.icp = icp;
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public String getRodC() {
        return rodC;
    }

    public void setRodC(String rodC) {
        this.rodC = rodC;
    }

    public Date getPomerOd() {
        return pomerOd;
    }

    public void setPomerOd(Date pomerOd) {
        this.pomerOd = pomerOd;
    }

    public Date getPomerDo() {
        return pomerDo;
    }

    public void setPomerDo(Date pomerDo) {
        this.pomerDo = pomerDo;
    }

    public String getStred() {
        return stred;
    }

    public void setStred(String stred) {
        this.stred = stred;
    }

    public String getVedIcp() {
        return vedIcp;
    }

    public void setVedIcp(String vedIcp) {
        this.vedIcp = vedIcp;
    }

    public String getVedJmeno() {
        return vedJmeno;
    }

    public void setVedJmeno(String vedJmeno) {
        this.vedJmeno = vedJmeno;
    }
}

package com.sar.server.models.api;

import java.util.Date;

public class Zapis {

    private String druh;
    private String kod;
    private String poznamka;
    private Date casZapisu;
    private String kodPra;

    public String getDruh() {
        return druh;
    }

    public void setDruh(String druh) {
        this.druh = druh;
    }

    public String getKod() {
        return kod;
    }

    public void setKod(String kod) {
        this.kod = kod;
    }

    public String getPoznamka() {
        return poznamka;
    }

    public void setPoznamka(String poznamka) {
        this.poznamka = poznamka;
    }

    public Date getCasZapisu() {
        return casZapisu;
    }

    public void setCasZapisu(Date casZapisu) {
        this.casZapisu = casZapisu;
    }

    public String getKodPra() {
        return kodPra;
    }

    public void setKodPra(String kodPra) {
        this.kodPra = kodPra;
    }
}

package com.sar.server.models.vo;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@Table(	name = "vyhodnoceni",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "id")
        })

public class VyhodnoceniVO {
        @Id
        @Column(name = "id")
        @GeneratedValue(strategy=GenerationType.AUTO)
        private Long id;

        @NotBlank
        @Column(name = "icp", nullable = false)
        private String icp;

        @NotBlank
        @Column(name = "datum", nullable = false)
        private Date datum;

        @NotBlank
        @Column(name = "kod_doby", nullable = false)
        private String kod_doby;

        @NotBlank
        @Column(name = "poc_hod", nullable = false)
        private float poc_hod;

        public String getIcp() {
                return icp;
        }

        public void setIcp(String icp) {
                this.icp = icp;
        }

        public Date getDatum() {
                return datum;
        }

        public void setDatum(Date datum) {
                this.datum = datum;
        }

        public String getKod_doby() {
                return kod_doby;
        }

        public void setKod_doby(String kod_doby) {
                this.kod_doby = kod_doby;
        }

        public float getPoc_hod() {
                return poc_hod;
        }

        public void setPoc_hod(float poc_hod) {
                this.poc_hod = poc_hod;
        }

        public Long getId() {return id;}

        public void setId(Long id) {
                this.id = id;
        }
}

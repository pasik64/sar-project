package com.sar.server.models.api;

public class Kontakt {

    private boolean isPritomen;
    private String jmeno;
    private String telCislo;

    public boolean isPritomen() {
        return isPritomen;
    }

    public void setPritomen(boolean pritomen) {
        isPritomen = pritomen;
    }

    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public String getTelCislo() {
        return telCislo;
    }

    public void setTelCislo(String telCislo) {
        this.telCislo = telCislo;
    }
}

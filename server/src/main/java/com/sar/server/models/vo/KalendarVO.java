package com.sar.server.models.vo;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@Table(	name = "kalendar",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "datum")
        })
public class KalendarVO {
    @Id
    @Column(name = "datum", nullable = false)
    private Date datum;

    @NotBlank
    @Column(name = "druh_dne", nullable = false)
    private String druh_dne;

    @NotBlank
    @Column(name = "svatek", nullable = false)
    private String svatek;

    @Column(name = "poznamka")
    private String poznamka;

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public String getDruh_dne() {
        return druh_dne;
    }

    public void setDruh_dne(String druh_dne) {
        this.druh_dne = druh_dne;
    }

    public String getSvatek() {
        return svatek;
    }

    public void setSvatek(String svatek) {
        this.svatek = svatek;
    }

    public String getPoznamka() {
        return poznamka;
    }

    public void setPoznamka(String poznamka) {
        this.poznamka = poznamka;
    }
}

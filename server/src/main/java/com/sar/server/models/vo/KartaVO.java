package com.sar.server.models.vo;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@Table(	name = "karta")

public class KartaVO {
    @Id
    @Column(name ="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @NotBlank
    @Column(name = "icp", nullable = false)
    private String icp;

    @NotBlank
    @Column(name = "datum", nullable = false)
    private Date datum;

    @NotBlank
    @Column(name = "kod_po", nullable = false)
    private String kod_po;

    @NotBlank
    @Column(name = "druh", nullable = false)
    private String druh;

    @NotBlank
    @Column(name = "cas", nullable = false)
    private int cas;

    @Column(name = "poznamka")
    private String poznamka;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIcp() {
        return icp;
    }

    public void setIcp(String icp) {
        this.icp = icp;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public String getKod_po() {
        return kod_po;
    }

    public void setKod_po(String kod_po) {
        this.kod_po = kod_po;
    }

    public String getDruh() {
        return druh;
    }

    public void setDruh(String druh) {
        this.druh = druh;
    }

    public int getCas() {
        return cas;
    }

    public void setCas(int cas) {
        this.cas = cas;
    }

    public String getPoznamka() {
        return poznamka;
    }

    public void setPoznamka(String poznamka) {
        this.poznamka = poznamka;
    }



}

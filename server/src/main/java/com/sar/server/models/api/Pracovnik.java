package com.sar.server.models.api;

public class Pracovnik {
    private String kodPra;

    public String getKodPra() {
        return kodPra;
    }

    public void setKodPra(String kodPra) {
        this.kodPra = kodPra;
    }
}

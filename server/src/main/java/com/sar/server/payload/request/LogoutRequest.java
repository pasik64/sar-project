package com.sar.server.payload.request;

import javax.validation.constraints.NotBlank;

public class LogoutRequest {
    @NotBlank
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

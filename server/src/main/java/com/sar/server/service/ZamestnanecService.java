package com.sar.server.service;

import com.sar.server.models.ZamestnanecWrapper;
import com.sar.server.models.api.Kontakt;
import com.sar.server.models.vo.ZamestnanecVO;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Service
public interface ZamestnanecService {

    public List<ZamestnanecVO> getZamestnanci();

    public List<Kontakt> getKontakty();

    public ZamestnanecWrapper findZamestnanecByIcp(String icp);

    public boolean zamestnanecPritomen(String icp) throws ParseException;

    String getZamestnanecStatus(Date date, String icp);
}

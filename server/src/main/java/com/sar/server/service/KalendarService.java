package com.sar.server.service;


import com.sar.server.models.vo.KalendarVO;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public interface KalendarService {
    public KalendarVO findByDatum(Date datum);
    public void setDruh_dne(Date datum, String druh_dne);
    public void setSvatek(Date datum, String svatek);
    public void setPoznamka(Date datum, String poznamka);
}

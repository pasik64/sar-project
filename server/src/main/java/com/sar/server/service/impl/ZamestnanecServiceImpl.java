package com.sar.server.service.impl;

import com.sar.server.enums.EnumDruhPruchodu;
import com.sar.server.models.ZamestnanecWrapper;
import com.sar.server.models.api.Kontakt;
import com.sar.server.models.vo.KartaVO;
import com.sar.server.models.vo.ZamestnanecVO;
import com.sar.server.repository.KartaRepository;
import com.sar.server.repository.ZamestnanecRepository;
import com.sar.server.service.ZamestnanecService;
import com.sar.server.utils.TimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ZamestnanecServiceImpl implements ZamestnanecService {

    @Autowired
    private ZamestnanecRepository zamestnanecRepository;

    @Autowired
    private KartaRepository kartaRepository;

    public List<ZamestnanecVO> getZamestnanci() {

        return zamestnanecRepository.findAll();
    }

    @Override
    public List<Kontakt> getKontakty() {
        List<Kontakt> kontakty = new ArrayList<>();
        List<ZamestnanecVO> zamestnanecVOList = zamestnanecRepository.findAll();
        for (ZamestnanecVO zamestnanecVO : zamestnanecVOList) {
            Kontakt kontakt = new Kontakt();
            kontakt.setJmeno(zamestnanecVO.getJmeno());
            kontakt.setPritomen(zamestnanecPritomen(zamestnanecVO.getIcp()));
            //TODO telCislo

            kontakty.add(kontakt);
        }


        return kontakty;
    }

    @Override
    public ZamestnanecWrapper findZamestnanecByIcp(String icp) {
        ZamestnanecVO zamestnanecVO = zamestnanecRepository.getByIcp(icp);
        ZamestnanecWrapper zamestnanecWrapper = new ZamestnanecWrapper();
        zamestnanecWrapper.setIcp(zamestnanecVO.getIcp());
        zamestnanecWrapper.setJmeno(zamestnanecVO.getJmeno());
        zamestnanecWrapper.setStred(zamestnanecVO.getStred());
        zamestnanecWrapper.setPomerDo(zamestnanecVO.getPomerDo());
        zamestnanecWrapper.setPomerOd(zamestnanecVO.getPomerOd());
        zamestnanecWrapper.setRodC(zamestnanecVO.getRodC());
        ZamestnanecVO vedouci = zamestnanecRepository.getByIcp(zamestnanecVO.getIcpVed());

        if (vedouci != null) {
            zamestnanecWrapper.setVedIcp(vedouci.getIcp());
            zamestnanecWrapper.setVedJmeno(vedouci.getJmeno());
        }
        return zamestnanecWrapper;
    }

    @Override
    public boolean zamestnanecPritomen(String icp) {
        SimpleDateFormat formatter = new SimpleDateFormat(
                "dd/MM/yyyy");
        List<KartaVO> kartaVOList = new ArrayList<>();
        try {
            kartaVOList = kartaRepository.findAllByDatumAndIcpOrderByCasDesc(formatter.parse(formatter.format(new Date())), icp);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (kartaVOList.size() > 0) {
            return kartaVOList.get(0).getDruh().equals(EnumDruhPruchodu.PRICHOD.kod);
        }
        return false;
    }

    @Override
    public String getZamestnanecStatus(Date date, String icp) {

        List<KartaVO> kartaVOList = kartaRepository.findAllByDatumAndIcpOrderByCasAsc(date, icp);
        StringBuilder status = new StringBuilder();
        EnumDruhPruchodu lastTyp = null;
        for (KartaVO karta : kartaVOList) {
            if (karta.getDruh().equals(EnumDruhPruchodu.PRICHOD.kod)) {
                lastTyp = EnumDruhPruchodu.PRICHOD;
                status.append(", ").append(TimeUtils.getTimeFromMinutes(karta.getCas()));
            } else {
                if (lastTyp == null || lastTyp.equals(EnumDruhPruchodu.ODCHOD)) {
                    status.append(", ").append(TimeUtils.getTimeFromMinutes(karta.getCas()));
                } else {
                    status.append(" - ").append(TimeUtils.getTimeFromMinutes(karta.getCas()));
                }
                lastTyp = EnumDruhPruchodu.ODCHOD;
            }
        }
        status.replace(0, 2, "");
        return status.toString();
    }
}

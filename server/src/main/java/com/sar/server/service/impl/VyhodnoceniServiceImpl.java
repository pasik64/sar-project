package com.sar.server.service.impl;

import com.sar.server.enums.EnumDruhPruchodu;
import com.sar.server.enums.EnumKodPo;
import com.sar.server.enums.EnumMzdovyKod;
import com.sar.server.models.vo.KartaVO;
import com.sar.server.models.vo.VyhodnoceniVO;
import com.sar.server.repository.VyhodnoceniRepository;
import com.sar.server.service.VyhodnoceniService;
import com.sar.server.utils.TimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@Service
public class VyhodnoceniServiceImpl implements VyhodnoceniService {

    @Autowired
    private VyhodnoceniRepository vyhodnoceniRepository;

    @Override
    public List<VyhodnoceniVO> getVyhodnoceni() {
        return vyhodnoceniRepository.findAll();
    }

    @Override
    public List<VyhodnoceniVO> findVyhodnoceniByIcpAndDatum(String icp, Date datum) throws ParseException {
        return vyhodnoceniRepository.getByIcpAndDatum(icp, TimeUtils.getMidnightDate(datum));
    }

    @Override
    public VyhodnoceniVO updateVyhodnoceni(VyhodnoceniVO vyhodnoceniVO) {
        VyhodnoceniVO vyhodnoceniOld = vyhodnoceniRepository.getById(vyhodnoceniVO.getId());
        LocalDate localDate = vyhodnoceniVO.getDatum().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Instant dayInst = localDate.atTime(0, 0).toLocalDate().atStartOfDay(ZoneId.systemDefault()).toInstant();
        vyhodnoceniOld.setDatum(Date.from(dayInst));
        vyhodnoceniOld.setIcp(vyhodnoceniVO.getIcp());
        vyhodnoceniOld.setKod_doby(vyhodnoceniVO.getKod_doby());
        vyhodnoceniOld.setPoc_hod(vyhodnoceniVO.getPoc_hod());
        return vyhodnoceniRepository.save(vyhodnoceniOld);
    }

    @Override
    public VyhodnoceniVO createVyhodnoceni(VyhodnoceniVO vyhodnoceniVO) {
        LocalDate localDate = vyhodnoceniVO.getDatum().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Instant dayInst = localDate.atTime(0, 0).toLocalDate().atStartOfDay(ZoneId.systemDefault()).toInstant();
        vyhodnoceniVO.setDatum(Date.from(dayInst));

        return vyhodnoceniRepository.save(vyhodnoceniVO);
    }

    @Override
    public void deleteVyhodnoceni(long vyhodnoceniId) {
        vyhodnoceniRepository.deleteById(vyhodnoceniId);
    }

    @Override
    public List<VyhodnoceniVO> generateVyhodnoceni(List<KartaVO> kartaVOList) {
        List<VyhodnoceniVO> vyhodnoceniVOList = new ArrayList<>();
        Map<String, Integer> vyhodnoceniMap = new HashMap<>();

        if (kartaVOList.size() < 2 || !kartaVOList.get(0).getDruh().equals(EnumDruhPruchodu.PRICHOD.kod)) {
            return vyhodnoceniVOList;
        }

        for (int i = 1; i < kartaVOList.size(); i++) {
            KartaVO currentKarta = kartaVOList.get(i);
            KartaVO previousKarta = kartaVOList.get(i - 1);
            if (currentKarta.getDruh().equals(EnumDruhPruchodu.ODCHOD.kod)) {
                if (!previousKarta.getDruh().equals(EnumDruhPruchodu.PRICHOD.kod)) {
                    return vyhodnoceniVOList;
                }
            } else {
                if (!previousKarta.getDruh().equals(EnumDruhPruchodu.ODCHOD.kod)) {
                    return vyhodnoceniVOList;
                }
            }

            if (!(currentKarta.getDruh().equals(EnumDruhPruchodu.PRICHOD.kod) && previousKarta.getKod_po().equals(EnumKodPo.STANDART.kod))) {
                if (vyhodnoceniMap.containsKey(previousKarta.getKod_po())) {
                    vyhodnoceniMap.replace(previousKarta.getKod_po(), vyhodnoceniMap.get(previousKarta.getKod_po()) + (currentKarta.getCas() - previousKarta.getCas()));
                } else {
                    vyhodnoceniMap.put(previousKarta.getKod_po(), (currentKarta.getCas() - previousKarta.getCas()));
                }
            }
        }

        vyhodnoceniRepository.deleteByDatum(kartaVOList.get(0).getDatum());

        for (String kod : new ArrayList<>(vyhodnoceniMap.keySet())) {
            VyhodnoceniVO vyhodnoceniVO = new VyhodnoceniVO();
            vyhodnoceniVO.setKod_doby(EnumMzdovyKod.getMzdovyKodByKodPo(kod));
            vyhodnoceniVO.setDatum(kartaVOList.get(0).getDatum());
            vyhodnoceniVO.setIcp(kartaVOList.get(0).getIcp());
            vyhodnoceniVO.setPoc_hod(Math.round((vyhodnoceniMap.get(kod) / 60.0f) * 100) / 100.0f);
            vyhodnoceniRepository.save(vyhodnoceniVO);
            vyhodnoceniVOList.add(vyhodnoceniVO);
        }


        return vyhodnoceniVOList;
    }

}

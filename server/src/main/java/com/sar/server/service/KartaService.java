package com.sar.server.service;

import com.sar.server.enums.EnumDruhPruchodu;
import com.sar.server.enums.EnumKodPo;
import com.sar.server.models.api.Zapis;
import com.sar.server.models.vo.KartaVO;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Service
public interface KartaService {

    public List<KartaVO> findAllByDatumAndIcp(Date datum, String icp) throws ParseException;

    public KartaVO updateKarta(KartaVO kartaVO);

    public KartaVO createKarta(KartaVO kartaVO);

    public void deleteKarta(long kartaId);

    public KartaVO zamestnanecPruchod(String icp, EnumDruhPruchodu druhPruchodu, EnumKodPo kodPo, String poznamka);

    public KartaVO zamestnanecPruchod(String icp, EnumDruhPruchodu druhPruchodu, EnumKodPo kodPo);

    KartaVO createVlastniZapis(Zapis zapis);
}

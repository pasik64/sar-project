package com.sar.server.service;


import com.sar.server.models.vo.KartaVO;
import com.sar.server.models.vo.VyhodnoceniVO;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Service
public interface VyhodnoceniService {
    public List<VyhodnoceniVO> getVyhodnoceni();

    public List<VyhodnoceniVO> findVyhodnoceniByIcpAndDatum(String icp, Date datum) throws ParseException;

    public VyhodnoceniVO updateVyhodnoceni(VyhodnoceniVO vyhodnoceniVO);

    public VyhodnoceniVO createVyhodnoceni(VyhodnoceniVO vyhodnoceniVO);

    public void deleteVyhodnoceni(long vyhodnoceniId);

    List<VyhodnoceniVO> generateVyhodnoceni(List<KartaVO> kartaVOList);
}

package com.sar.server.service.impl;

import com.sar.server.enums.EnumDruhPruchodu;
import com.sar.server.enums.EnumKodPo;
import com.sar.server.models.api.Zapis;
import com.sar.server.models.vo.KartaVO;
import com.sar.server.repository.KartaRepository;
import com.sar.server.service.KartaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class KartaServiceImpl implements KartaService {

    @Autowired
    KartaRepository kartaRepository;

    @Override
    public List<KartaVO> findAllByDatumAndIcp(Date datum, String icp) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(
                "dd-MM-yyyy");
        List<KartaVO> kartaVOList =  kartaRepository.findAllByDatumAndIcp(formatter.parse(formatter.format(datum)), icp);

        kartaVOList.sort((o1, o2) -> {
            if (o1.getCas() > o2.getCas()) {
                return 1;
            }
            else if (o1.getCas() == o2.getCas()) {
                if (o1.getId() > o2.getId()) {
                    return 1;
                } else return -1;
            } else return -1;
        });

        return kartaVOList;
    }

    @Override
    public KartaVO createKarta(KartaVO kartaVO) {

        //nastavení času u datumu na půlnoc
        LocalDate localDate = kartaVO.getDatum().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Instant dayInst = localDate.atTime(0, 0).toLocalDate().atStartOfDay(ZoneId.systemDefault()).toInstant();
        kartaVO.setDatum(Date.from(dayInst));


        return kartaRepository.save(kartaVO);
    }

    @Override
    public void deleteKarta(long kartaId) {
        kartaRepository.deleteById(kartaId);
    }

    @Override
    public KartaVO zamestnanecPruchod(String icp, EnumDruhPruchodu druhPruchodu, EnumKodPo kodPo, String poznamka) {
        KartaVO kartaVO = new KartaVO();
        kartaVO.setPoznamka(poznamka);
        kartaVO.setIcp(icp);
        kartaVO.setDruh(druhPruchodu.kod);
        kartaVO.setKod_po(kodPo.kod);
        int hours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int minutes = Calendar.getInstance().get(Calendar.MINUTE);
        int time = hours * 60 + minutes;
        kartaVO.setCas(time);
        SimpleDateFormat formatter = new SimpleDateFormat(
                "dd/MM/yyyy");
        try {
            kartaVO.setDatum(formatter.parse(formatter.format(new Date())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        kartaRepository.save(kartaVO);
        return kartaVO;
    }
    @Override
    public KartaVO zamestnanecPruchod(String icp, EnumDruhPruchodu druhPruchodu, EnumKodPo kodPo) {
        return zamestnanecPruchod(icp, druhPruchodu, kodPo, "");
    }

    @Override
    public KartaVO createVlastniZapis(Zapis zapis) {
        KartaVO kartaVO = new KartaVO();

        kartaVO.setDruh(zapis.getDruh());
        kartaVO.setIcp(zapis.getKodPra());
        kartaVO.setKod_po(zapis.getKod());
        kartaVO.setDruh(zapis.getDruh());
        LocalDate localDate = zapis.getCasZapisu().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Instant dayInst = localDate.atTime(0, 0).toLocalDate().atStartOfDay(ZoneId.systemDefault()).toInstant();
        kartaVO.setDatum(Date.from(dayInst));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(zapis.getCasZapisu());
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);
        int time = hours * 60 + minutes;
        kartaVO.setCas(time);
        kartaRepository.save(kartaVO);

        return kartaVO;
    }

    @Override
    public KartaVO updateKarta(KartaVO kartaVO) {
        KartaVO kartaOld = kartaRepository.getById(kartaVO.getId());

        kartaOld.setKod_po(kartaVO.getKod_po());
        kartaOld.setIcp(kartaVO.getIcp());
        kartaOld.setDruh(kartaVO.getDruh());
        LocalDate localDate = kartaVO.getDatum().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Instant dayInst = localDate.atTime(0, 0).toLocalDate().atStartOfDay(ZoneId.systemDefault()).toInstant();
        kartaOld.setDatum(Date.from(dayInst));
        kartaOld.setCas(kartaVO.getCas());
        kartaOld.setPoznamka(kartaVO.getPoznamka());

        return kartaRepository.save(kartaOld);
    }
}

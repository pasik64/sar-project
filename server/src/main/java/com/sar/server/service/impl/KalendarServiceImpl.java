package com.sar.server.service.impl;


import com.sar.server.models.vo.KalendarVO;
import com.sar.server.repository.KalendarRepository;
import com.sar.server.service.KalendarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Service
public class KalendarServiceImpl implements KalendarService {

    @Autowired
    KalendarRepository kalendarRepository;

    @Override
    public KalendarVO findByDatum(Date datum) {
        LocalDate localDate = datum.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Instant dayInst = localDate.atTime(0, 0).toLocalDate().atStartOfDay(ZoneId.systemDefault()).toInstant();
        return kalendarRepository.findByDatum(Date.from(dayInst));
    }

    @Override
    public void setDruh_dne(Date datum, String druh_dne) {
        KalendarVO kalendarVO = kalendarRepository.findByDatum(datum);
        kalendarVO.setDruh_dne(druh_dne);
        kalendarRepository.save(kalendarVO);
    }

    @Override
    public void setSvatek(Date datum, String svatek) {
        KalendarVO kalendarVO = kalendarRepository.findByDatum(datum);
        kalendarVO.setSvatek(svatek);
        kalendarRepository.save(kalendarVO);
    }

    @Override
    public void setPoznamka(Date datum, String poznamka) {
        KalendarVO kalendarVO = kalendarRepository.findByDatum(datum);
        kalendarVO.setPoznamka(poznamka);
        kalendarRepository.save(kalendarVO);
    }
}

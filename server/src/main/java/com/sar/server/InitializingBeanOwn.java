package com.sar.server;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import com.sar.server.models.vo.User;
import com.sar.server.repository.UserRepository;

@Component
public class InitializingBeanOwn implements InitializingBean {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Override
    public void afterPropertiesSet() {
        Boolean adminExists = userRepository.existsByName("1");

        if(!adminExists){
            User user = new User("1", encoder.encode("admin"));
            userRepository.save(user);
        }
    }
}
package com.sar.server.repository;

import com.sar.server.models.vo.KartaVO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Repository
public interface KartaRepository extends JpaRepository<KartaVO, Long> {

    @Transactional
    public void deleteById(long id);

    public List<KartaVO> findAllByDatumAndIcp(Date date, String icp);

    public List<KartaVO> findAllByDatumAndIcpOrderByCasDesc(Date date, String icp);

    public List<KartaVO> findAllByDatumAndIcpOrderByCasAsc(Date date, String icp);
}

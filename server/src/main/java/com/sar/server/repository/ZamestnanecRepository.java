package com.sar.server.repository;

import com.sar.server.models.vo.ZamestnanecVO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZamestnanecRepository extends JpaRepository<ZamestnanecVO, Long> {

    public ZamestnanecVO getByIcp(String icp);
}

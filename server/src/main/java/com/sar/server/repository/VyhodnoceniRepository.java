package com.sar.server.repository;


import com.sar.server.models.vo.VyhodnoceniVO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Repository
public interface VyhodnoceniRepository extends JpaRepository<VyhodnoceniVO, Long> {
    public List<VyhodnoceniVO> getByIcpAndDatum(String icp, Date datum);

    @Transactional
    public void deleteById(Long id);

    @Transactional
    public void deleteByDatum(Date datum);
}

package com.sar.server.repository;

import com.sar.server.models.vo.KalendarVO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface KalendarRepository extends JpaRepository<KalendarVO, Long>{
    public KalendarVO findByDatum(Date datum);
}

package com.sar.server.controller;

import com.sar.server.models.vo.KartaVO;
import com.sar.server.service.KartaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*")
@Controller
@RestController
@RequestMapping("/api/karta")
public class KartaController {

    @Autowired
    private KartaService kartaService;

    @GetMapping("/byDatum")
    public List<KartaVO> findKartaByDatumAndIcp(@RequestParam("datum")  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date datum, @RequestParam("icp") String icp) throws ParseException {
        return kartaService.findAllByDatumAndIcp(datum, icp);
    }

    @PostMapping("/new")
    public KartaVO createKarta(@RequestBody KartaVO karta) {
        return kartaService.createKarta(karta);
    }

    @PostMapping("/update")
    public KartaVO updateKarta(@RequestBody KartaVO karta) {
        return kartaService.updateKarta(karta);
    }

    @GetMapping("/delete")
    public void deleteKarta(@RequestParam("id") long kartaId) {
        kartaService.deleteKarta(kartaId);
    }
}

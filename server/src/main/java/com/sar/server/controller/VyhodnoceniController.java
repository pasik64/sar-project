package com.sar.server.controller;

import com.sar.server.enums.EnumDruhPruchodu;
import com.sar.server.enums.EnumKodPo;
import com.sar.server.models.vo.KartaVO;
import com.sar.server.models.vo.VyhodnoceniVO;
import com.sar.server.service.KartaService;
import com.sar.server.service.VyhodnoceniService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.*;

@CrossOrigin(origins = "*")
@Controller
@RestController
@RequestMapping("/api/vyhodnoceni")
public class VyhodnoceniController {
    @Autowired
    public VyhodnoceniService vyhodnoceniService;

    @Autowired
    public KartaService kartaService;

    @GetMapping("/")
    public List<VyhodnoceniVO> getVyhodnoceni() {
        return vyhodnoceniService.getVyhodnoceni();
    }

    @GetMapping("/byIcpAndDatum")
    public List<VyhodnoceniVO> findVyhodnoceniByIcpAndDatum(@RequestParam("icp") String icp, @RequestParam("datum")  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date datum) throws ParseException {
        return vyhodnoceniService.findVyhodnoceniByIcpAndDatum(icp, datum);
    }

    @PostMapping("/new")
    public VyhodnoceniVO createVyhodnoceni(@RequestBody VyhodnoceniVO vyhodnoceniVO) {
        return vyhodnoceniService.createVyhodnoceni(vyhodnoceniVO);
    }

    @PostMapping("/update")
    public VyhodnoceniVO updateKarta(@RequestBody VyhodnoceniVO vyhodnoceniVO) {
        return vyhodnoceniService.updateVyhodnoceni(vyhodnoceniVO);
    }

    @GetMapping("/delete")
    public void deleteVyhodnoceni(@RequestParam("id") long vyhodnoceniId) {
        vyhodnoceniService.deleteVyhodnoceni(vyhodnoceniId);
    }

    @GetMapping("/generate")
    public List<VyhodnoceniVO> generateVyhodnoceni(@RequestParam("icp") String icp,
                                             @RequestParam("datum") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date) throws ParseException {
        List<KartaVO> kartaVOList = kartaService.findAllByDatumAndIcp(date, icp);

        return vyhodnoceniService.generateVyhodnoceni(kartaVOList);
    }
}

package com.sar.server.controller;

import com.sar.server.models.ZamestnanecWrapper;
import com.sar.server.models.vo.ZamestnanecVO;
import com.sar.server.service.ZamestnanecService;
import com.sar.server.utils.TimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*")
@Controller
@RestController
@RequestMapping("/api/zamestnanci")
public class ZamestnanecController {

    @Autowired
    public ZamestnanecService zamestnanecService;

    @GetMapping("/")
    public List<ZamestnanecVO> getZamestnanci() {
        return zamestnanecService.getZamestnanci();
    }

    @GetMapping("/{icp}")
    public ZamestnanecWrapper findZamestnanecByIcp(@PathVariable("icp") String icp) {
        return zamestnanecService.findZamestnanecByIcp(icp);
    }
}

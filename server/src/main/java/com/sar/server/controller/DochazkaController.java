package com.sar.server.controller;

import com.sar.server.enums.EnumDruhPruchodu;
import com.sar.server.enums.EnumKodPo;
import com.sar.server.models.api.Kontakt;
import com.sar.server.models.api.Zapis;
import com.sar.server.models.vo.KartaVO;
import com.sar.server.service.KartaService;
import com.sar.server.service.ZamestnanecService;
import com.sar.server.utils.TimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class DochazkaController {

    @Autowired
    private ZamestnanecService zamestnanecService;

    @Autowired
    private KartaService kartaService;

    @PostMapping("/dochazka/status")
    public String getPracovnikStatus(@RequestParam("KodPra") String icp, @RequestParam(value = "Datum", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date datum) {
        if (datum == null) {
            return zamestnanecService.getZamestnanecStatus(TimeUtils.getCurrentDateWithoutMinutes(), icp);
        } else {
            return zamestnanecService.getZamestnanecStatus(TimeUtils.getMidnightDate(datum), icp);
        }
    }

    @PostMapping("/dochazka/vlastni-zapis")
    public boolean createVlastniZapis(@RequestBody Zapis zapis) {
        KartaVO kartaVO = kartaService.createVlastniZapis(zapis);
        return kartaVO != null;
    }

    @PostMapping("/odchod/obed")
    public boolean odchodObed(@RequestParam("KodPra") String icp)  {
        KartaVO kartaVO = kartaService.zamestnanecPruchod(icp, EnumDruhPruchodu.ODCHOD, EnumKodPo.OBED);
        return  kartaVO != null;
    }

    @PostMapping("/odchod/lekar")
    public boolean odchodLekar(@RequestParam("KodPra") String icp) {
        KartaVO kartaVO = kartaService.zamestnanecPruchod(icp, EnumDruhPruchodu.ODCHOD, EnumKodPo.LEKAR);
        return  kartaVO != null;
    }

    @PostMapping("/odchod")
    public boolean odchod(@RequestParam("KodPra") String icp) {
        KartaVO kartaVO = kartaService.zamestnanecPruchod(icp, EnumDruhPruchodu.ODCHOD, EnumKodPo.STANDART);
        return  kartaVO != null;
    }

    @PostMapping("/prichod/home-office")
    public boolean prichodHO(@RequestParam("KodPra") String icp) {
        KartaVO kartaVO = kartaService.zamestnanecPruchod(icp, EnumDruhPruchodu.PRICHOD, EnumKodPo.STANDART, "home-office");
        return  kartaVO != null;
    }

    @PostMapping("/prichod")
    public boolean prichod(@RequestParam("KodPra") String icp) {
        KartaVO kartaVO = kartaService.zamestnanecPruchod(icp, EnumDruhPruchodu.PRICHOD, EnumKodPo.STANDART);
        return  kartaVO != null;
    }

    @GetMapping("/Kontakty")
    public List<Kontakt> getKontakty() {
        return zamestnanecService.getKontakty();
    }



}

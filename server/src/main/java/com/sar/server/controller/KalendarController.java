package com.sar.server.controller;

import com.sar.server.models.vo.KalendarVO;
import com.sar.server.service.KalendarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Date;

@CrossOrigin(origins = "*")
@Controller
@RestController
@RequestMapping("/api/kalendar")
public class KalendarController {
    @Autowired
    private KalendarService kalendarService;

    @GetMapping("/byDatum")
    public KalendarVO findKalendarByDatum(@RequestParam("datum") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date datum) throws ParseException {
        return kalendarService.findByDatum(datum);
    }
}

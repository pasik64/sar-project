package com.sar.server.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import com.sar.server.payload.request.LoginRequest;
import com.sar.server.payload.request.LogoutRequest;
import com.sar.server.payload.response.JwtResponse;
import com.sar.server.repository.UserRepository;
import com.sar.server.security.jwt.JwtUtils;
import com.sar.server.security.services.UserDetailsImpl;

import java.util.ArrayList;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    public static ArrayList<UserDetailsImpl> userList = new ArrayList<UserDetailsImpl>();

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(loginRequest.getName(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        if(! userList.contains(userDetails)){
            userList.add(userDetails);
        }

        return ResponseEntity.ok(new JwtResponse(jwt,
            userDetails.getId(),
            userDetails.getName()));
    }

    @PostMapping("/logout")
    public void logoutUser(@Valid @RequestBody LogoutRequest logoutRequest) {
        String logout = logoutRequest.getName();
        for(int i = 0; i < userList.size(); i++){
            String user = userList.get(i).getName();
            if(user.equals(logout)){
                userList.remove(i);
            }
        }
    }
}

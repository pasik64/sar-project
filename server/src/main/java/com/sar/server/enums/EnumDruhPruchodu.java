package com.sar.server.enums;

public enum EnumDruhPruchodu {
    PRICHOD("P"),
    ODCHOD("O");

    public final String kod;

    EnumDruhPruchodu(String kod) {
        this.kod = kod;
    }
}

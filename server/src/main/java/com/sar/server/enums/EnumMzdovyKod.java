package com.sar.server.enums;

public enum EnumMzdovyKod {
    ZAKL("ZÁKL"),
    OBED("OBĚD"),
    LEKAR("LÉKAŘ"),
    CESTA("CESTA");

    public final String kod;

    EnumMzdovyKod(String kod) {
        this.kod = kod;
    }

    public static String getMzdovyKodByKodPo(String kodPo) {
        switch (kodPo) {
            case "00" : return ZAKL.kod;
            case "01" : return CESTA.kod;
            case "02" : return OBED.kod;
            case "03" : return LEKAR.kod;
            default: return null;
        }
    }
}

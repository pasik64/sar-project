package com.sar.server.enums;

public enum EnumKodPo {
    STANDART("00"),
    SLUZEBNI_CESTA("01"),
    OBED("02"),
    LEKAR("03");

    public final String kod;

    EnumKodPo(String kod) {
        this.kod = kod;
    }
}

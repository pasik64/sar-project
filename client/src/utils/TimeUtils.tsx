export function getTime(value: string | number) {
  // @ts-ignore
  const division = value / 60;
  // @ts-ignore
  const modulo = value % 60;
  return Math.floor(division) + ":" + (modulo < 10 ? "0" + modulo : modulo);
}

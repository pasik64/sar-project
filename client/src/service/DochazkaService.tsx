import axios from "axios";

const API_URL = "http://localhost:8080/api/dochazka/";

class DochazkaService {
  getPracovnikStatus(icp: string | null, date: Date | null) {
    return axios.post(API_URL + "status", null, {
      params: {
        KodPra: icp,
        Datum: date
      }
    });
  }
}

export default new DochazkaService();

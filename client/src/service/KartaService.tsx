import axios from "axios";
import { KartaVO } from "../VO/KartaVO";

const API_URL = "http://localhost:8080/api/karta/";

class KartaService {
  getAll(datum: Date, icp: string) {
    return axios.get(API_URL + "byDatum", {
      params: {
        datum: datum,
        icp: icp,
      },
    });
  }

  createKarta(karta: KartaVO) {
    return axios.post(API_URL + "new", karta);
  }

  updateKarta(karta: KartaVO) {
    return axios.post(API_URL + "update", karta);
  }

  deleteKarta(id: string) {
    return axios.get(API_URL + "delete", {
      params: {
        id: id,
      },
    });
  }
}

export default new KartaService();

import axios from "axios";

const API_URL = "http://localhost:8080/api/auth/";

class AuthService {
  login(name: string, password: string) {
    return axios
      .post(API_URL + "login", {
        name,
        password,
      })
      .then((response) => {
        if (response.data.accessToken) {
          localStorage.setItem("user", JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  logout(name: string) {
    axios.post(API_URL + "logout", { name }).then(() => {
      localStorage.removeItem("user");
      window.location.reload();
    });
  }

  getCurrentUser() {
    // @ts-ignore
    return JSON.parse(localStorage.getItem("user"));
  }
}

export default new AuthService();

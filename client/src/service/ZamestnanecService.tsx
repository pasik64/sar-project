import axios from "axios";

const API_URL = "http://localhost:8080/api/zamestnanci/";

class ZamestnanecService {
  getZamestnanci() {
    return axios.get(API_URL);
  }

  findZamestnanecByIcp(icp: string) {
    return axios.get(API_URL + icp);
  }
}

export default new ZamestnanecService();

import axios from "axios";
import { VyhodnoceniVO } from "../VO/VyhodnoceniVO";

const API_URL = "http://localhost:8080/api/vyhodnoceni/";

class VyhodnoceniService {
  getVyhodnoceni() {
    return axios.get(API_URL);
  }

  findVyhodnoceniByIcpAndDatum(icp: string, datum: Date) {
    return axios.get(API_URL + "byIcpAndDatum", {
      params: {
        datum: datum,
        icp: icp,
      },
    });
  }

  createVyhodnoceni(vyhodnoceni: VyhodnoceniVO) {
    return axios.post(API_URL + "new", vyhodnoceni);
  }

  updateVyhodnoceni(vyhodnoceni: VyhodnoceniVO) {
    return axios.post(API_URL + "update", vyhodnoceni);
  }

  deleteVyhodnoceni(id: string) {
    return axios.get(API_URL + "delete", {
      params: {
        id: id,
      },
    });
  }

  generateVyhodnoceni(icp: string | null, datum: Date | null) {
    return axios.get(API_URL + "generate", {
      params: {
        icp: icp,
        datum: datum,
      },
    });
  }
}

export default new VyhodnoceniService();

import axios from "axios";

const API_URL = "http://localhost:8080/api/kalendar/";

class KalendarService {
  findKalendarByDatum(datum: Date) {
    return axios.get(API_URL + "byDatum", {
      params: {
        datum: datum,
      },
    });
  }
}

export default new KalendarService();

import React, { useState, useEffect } from "react";
import { Col, Row } from "react-bootstrap";
import ZamestnanecService from "../service/ZamestnanecService";
import { ZamestnanecVO } from "../VO/ZamestnanecVO";

type Props = React.InputHTMLAttributes<HTMLInputElement> & {
  icp: string;
};
export const ZamestnanecOverview: React.FC<Props> = ({ icp }) => {
  const [data, setData] = useState<ZamestnanecVO | null>(null);

  useEffect(() => {
    ZamestnanecService.findZamestnanecByIcp(icp).then((resp) => {
      setData(resp.data);
    });
  }, [icp]);

  return (
    <>
      {data !== null ? (
        <>
          <Row>
            <Col sm={3}>
              <b>Zaměstnanec</b>
            </Col>
            <Col sm={3}>
              {data.jmeno}
              <i>{" (" + icp + ")"}</i>
            </Col>
            <Col sm={3}>
              <b>Rodné číslo</b>
            </Col>
            <Col sm={3}>{data.rodC}</Col>
          </Row>
          <Row>
            <Col sm={3}>
              <b>Vedoucí</b>
            </Col>
            <Col sm={3}>
              {data.vedouci ? (
                <>
                  {data.vedouci.jmeno}
                  <i>{" (" + data.vedouci.icp + ")"}</i>
                </>
              ) : (
                <>-</>
              )}
            </Col>
            <Col sm={3}>
              <b>Středisko/Pracoviště</b>
            </Col>
            <Col sm={3}>{data.stred}</Col>
          </Row>
        </>
      ) : (
        <b>Neznámý zaměstnanec</b>
      )}
    </>
  );
};

import React, { useState, useEffect } from "react";
import { Col, Row } from "react-bootstrap";
import KalendarService from "../service/KalendarService";
import { getCaption as getDruhDne } from "../enums/EnumDruhDne";
import { KalendarVO } from "../VO/KalendarVO";

type Props = React.InputHTMLAttributes<HTMLInputElement> & {
  datum: Date;
};
export const KalendarOverview: React.FC<Props> = ({ datum }) => {
  const [data, setData] = useState<KalendarVO | null>(null);

  useEffect(() => {
    KalendarService.findKalendarByDatum(datum).then((resp) => {
      setData(resp.data);
    });
  }, [datum]);

  return (
    <>
      {data !== null ? (
        <Row>
          <Col sm={2}>
            <b>Datum</b>
          </Col>
          <Col sm={2}>
            {datum.getDate() +
              ". " +
              (datum.getMonth() + 1) +
              ". " +
              datum.getFullYear()}
          </Col>
          <Col sm={2}>
            <b>Druh dne</b>
          </Col>
          <Col sm={2}>{getDruhDne(data.druh_dne)}</Col>
          <Col sm={2}>
            <b>Svátek</b>
          </Col>
          <Col sm={2}>{data.svatek}</Col>
        </Row>
      ) : (
        <b>Neznámé datum</b>
      )}
    </>
  );
};

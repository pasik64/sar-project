import React, { useState, useEffect } from "react";
import { Col, Row } from "react-bootstrap";
import DochazkaService from '../service/DochazkaService';
import {KartaVO} from '../VO/KartaVO';

type Props = React.InputHTMLAttributes<HTMLInputElement> & {
  icp: string | null;
  date: Date | null;
  karty: Array<KartaVO> | null;
};
export const KartaOverview: React.FC<Props> = ({ icp, date, karty }) => {
  const [data, setData] = useState<string | null>(null);

  useEffect(() => {
    DochazkaService.getPracovnikStatus(icp, date).then((resp) => {
      setData(resp.data);
    });
  }, [icp, date, karty]);


  return (
    <>
      {(data !== null && data.length > 0) ? (
        <>
          <Row>
            <Col>
              <b>Vykázáno:</b>
            </Col>
            <Col>
              {data}
            </Col>
          </Row>
        </>
      ) : (
        <i>Zatím nebyl přidán žádný záznam</i>
      )}
    </>
  );
};

export enum EnumDruhDne {
  S = "S",
  P = "P",
  V = "V",
}

const params = {
  [EnumDruhDne.S]: "Svátek",
  [EnumDruhDne.P]: "Pracovní",
  [EnumDruhDne.V]: "Volno",
};

export function getCaption(value: string) {
  // @ts-ignore
  return params[value];
}

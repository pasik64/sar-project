export enum EnumVyhodnoceni {
  ZAKL = "ZÁKL",
  OBED = "OBĚD",
  LEKAR = "LÉKAŘ",
  CESTA = "CESTA",
}

const params = {
  [EnumVyhodnoceni.ZAKL]: "Základní pracovní doba",
  [EnumVyhodnoceni.OBED]: "Oběd",
  [EnumVyhodnoceni.LEKAR]: "Čas u lékaře",
  [EnumVyhodnoceni.CESTA]: "Čas na pracovní cestě",
};

export function getCaption(value: string) {
  // @ts-ignore
  return params[value];
}

export enum EnumDruhPruchodu {
  P = "P",
  O = "O",
}

const params = {
  [EnumDruhPruchodu.P]: "Příchod",
  [EnumDruhPruchodu.O]: "Odchod",
};

export function getCaption(value: string) {
  // @ts-ignore
  return params[value];
}

export enum EnumKodPo {
  E00 = "00",
  E01 = "01",
  E02 = "02",
  E03 = "03",
}

const params = {
  [EnumKodPo.E00]: "Standardní",
  [EnumKodPo.E01]: "Služební cesta",
  [EnumKodPo.E02]: "Oběd",
  [EnumKodPo.E03]: "Lékař",
};

export function getCaption(value: string) {
  // @ts-ignore
  return params[value];
}

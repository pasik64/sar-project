export interface KalendarVO {
  datum: Date;
  druh_dne: string;
  svatek: string;
  poznamka?: string;
}

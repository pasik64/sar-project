export interface VyhodnoceniVO {
  id?: number;
  icp: string;
  datum: Date;
  kod_doby: string;
  poc_hod: number;
}

export interface KartaVO {
  id?: number;
  icp: string;
  datum: Date;
  kod_po: string;
  druh: string;
  cas: number;
  poznamka?: string;
}

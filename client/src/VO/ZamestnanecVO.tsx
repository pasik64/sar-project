export interface ZamestnanecVO {
  icp: string;
  jmeno: string;
  rodC: string;
  pomerOd: Date;
  pomer_do: Date;
  stred: string;
  vedouci: ZamestnanecVO;
}

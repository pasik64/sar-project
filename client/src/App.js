import React, { Component } from "react";
import { Navbar, Nav } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import "./App.css";
import Routes from "./Routes";
import AuthService from "./service/AuthService";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentUser: null,
    };
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();

    if (user) {
      this.setState({
        currentUser: user,
      });
    }
  }

  render() {
    return (
      <div className="App py-3">
        <Navbar
          collapseOnSelect
          bg="light"
          expand="md"
          className="mb-3 width-nvb"
        >
          <Navbar.Brand className="font-weight-bold text-muted mx-3" href="/">
            Docházkový systém
          </Navbar.Brand>
          <Navbar.Toggle />

          <Navbar.Collapse className="justify-content-end">
            <Nav activeKey={window.location.pathname}>
              {this.state.currentUser ? (
                <>
                  <b className={"mr-3 my-auto"}>
                    {this.state.currentUser.name}
                  </b>

                  <LinkContainer
                    to="/login"
                    onClick={() =>
                      AuthService.logout(this.state.currentUser.name)
                    }
                  >
                    <Nav.Link className={"mr-3"}>Logout</Nav.Link>
                  </LinkContainer>
                </>
              ) : (
                <>
                  <LinkContainer to="/login">
                    <Nav.Link className={"mr-3"}>Login</Nav.Link>
                  </LinkContainer>
                </>
              )}
            </Nav>
          </Navbar.Collapse>
        </Navbar>

        {/*Routers for url*/}
        <Routes logged={this.state.currentUser} />
      </div>
    );
  }
}

export default App;

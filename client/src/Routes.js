import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";

import NotFound from "./containers/NotFound";
import Login from "./containers/Login";
import { Home } from "./containers/Home";
import { Attendance } from "./containers/Attendance";

class Routes extends Component {
  render() {
    const { logged } = this.props;
    return (
      <Switch>
        <Route exact path="/">
          <Home logged={logged !== null} />
        </Route>
        {logged ? (
          <Route exact path="/attendance">
            <Attendance logged={logged} />
          </Route>
        ) : (
          <Route exact path="/login">
            <Login />
          </Route>
        )}
        <Route>
          <NotFound />
        </Route>
      </Switch>
    );
  }
}

export default Routes;

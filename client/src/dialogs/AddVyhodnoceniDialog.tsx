import React, { useEffect, useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Button, Form } from "react-bootstrap";
import {
  EnumVyhodnoceni,
  getCaption as enumVyhodnoceni_Caption,
} from "../enums/EnumVyhodnoceni";
import { VyhodnoceniVO } from "../VO/VyhodnoceniVO";
import VyhodnoceniService from "../service/VyhodnoceniService";
import * as NumericInput from "react-numeric-input";

type Props = React.InputHTMLAttributes<HTMLInputElement> & {
  title: string | null;
  open: boolean;
  date: Date;
  icp: string;
  handleClose: () => void;
  initialValues: VyhodnoceniVO | null;
};
export const AddVyhodnoceniDialog: React.FC<Props> = ({
  title,
  icp,
  date,
  open,
  handleClose,
  initialValues,
}) => {
  const [kod_doby, setKod_doby] = useState<string>(
    initialValues ? initialValues.kod_doby : "ZÁKL"
  );
  const [poc_hod, setPoc_hod] = useState<number>(
    initialValues ? initialValues.poc_hod : 0
  );

  useEffect(() => {
    if (initialValues !== null) {
      setKod_doby(initialValues.kod_doby);
      setPoc_hod(initialValues.poc_hod);
    }
  }, [initialValues]);

  const handleSubmit = () => {
    if (title === "Přidat záznam") {
      VyhodnoceniService.createVyhodnoceni({
        id: undefined,
        icp: icp,
        poc_hod: poc_hod,
        kod_doby: kod_doby,
        datum: date,
      }).then(() => {
        handleClose();
      });
    } else {
      const id = initialValues ? initialValues.id : -1;
      VyhodnoceniService.updateVyhodnoceni({
        id: id,
        icp: icp,
        poc_hod: poc_hod,
        kod_doby: kod_doby,
        datum: date,
      }).then(() => {
        handleClose();
      });
    }
  };

  // @ts-ignore
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <Form.Group size="lg" controlId="kod_po">
          <Form.Label>Vyhodnocený mzdový kód</Form.Label>
          <Form.Control
            as="select"
            value={kod_doby}
            onChange={(e) => setKod_doby(e.target.value)}
          >
            {Object.values(EnumVyhodnoceni).map((item) => (
              <option value={item} key={item}>
                {enumVyhodnoceni_Caption(item)}
              </option>
            ))}
          </Form.Control>
        </Form.Group>

        <Form.Group size="lg" controlId="druh">
          <Form.Label>Vyhodnocený počet hodin</Form.Label>
          <NumericInput
            className="form-control"
            min={0}
            max={24}
            value={poc_hod}
            onChange={(e) => setPoc_hod(e)}
          />
        </Form.Group>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => handleSubmit()}>Uložit</Button>
        <Button onClick={() => handleClose()}>Zrušit</Button>
      </DialogActions>
    </Dialog>
  );
};

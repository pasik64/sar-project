import React, { useEffect, useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Button, Form } from "react-bootstrap";
import { KartaVO } from "../VO/KartaVO";
import { Col, Row } from "react-bootstrap";
import { EnumKodPo, getCaption as enumKodPo_Caption } from "../enums/EnumKodPo";
import {
  EnumDruhPruchodu,
  getCaption as enumDruhPruchodu_Caption,
} from "../enums/EnumDruhPruchodu";
import KartaService from "../service/KartaService";

type Props = React.InputHTMLAttributes<HTMLInputElement> & {
  title: string | null;
  open: boolean;
  date: Date;
  icp: string;
  handleClose: () => void;
  initialValues: KartaVO | null;
};
export const AddKartaDialog: React.FC<Props> = ({
  title,
  icp,
  date,
  open,
  handleClose,
  initialValues,
}) => {
  const [kod_po, setKod_po] = useState<string>(
    initialValues ? initialValues.kod_po : "00"
  );
  const [druh, setDruh] = useState<string>(
    initialValues ? initialValues.druh : "P"
  );
  const [poznamka, setPoznamka] = useState<string>(
    initialValues && initialValues.poznamka ? initialValues.poznamka : ""
  );
  const [cas, setCas] = useState<string>(
    initialValues
      ? Math.floor(initialValues.cas / 60) +
          ":" +
          (initialValues.cas % 60 > 10
            ? initialValues.cas % 60
            : "0" + (initialValues.cas % 60))
      : "0:0"
  );

  useEffect(() => {
    if (initialValues !== null) {
      const initH =
        initialValues.cas / 60 > 10
          ? Math.floor(initialValues.cas / 60)
          : "0" + Math.floor(initialValues.cas / 60);
      const initM =
        initialValues.cas % 60 > 10
          ? initialValues.cas % 60
          : "0" + (initialValues.cas % 60);

      setKod_po(initialValues.kod_po);
      setDruh(initialValues.druh);
      setCas(initialValues ? initH + ":" + initM : "0:0");
      setPoznamka(
        initialValues && initialValues.poznamka ? initialValues.poznamka : ""
      );
    }
  }, [initialValues]);

  const handleSubmit = () => {
    const stringSplit = cas.split(":");
    const correctTime =
      parseInt(stringSplit[0]) * 60 + parseInt(stringSplit[1]);
    if (title === "Přidat záznam") {
      KartaService.createKarta({
        id: 0,
        icp: icp,
        cas: correctTime,
        kod_po: kod_po,
        druh: druh,
        poznamka: poznamka,
        datum: date,
      }).then(() => {
        handleClose();
      });
    } else {
      const id = initialValues ? initialValues.id : -1;
      KartaService.updateKarta({
        id: id,
        icp: icp,
        cas: correctTime,
        kod_po: kod_po,
        druh: druh,
        poznamka: poznamka,
        datum: date,
      }).then(() => {
        handleClose();
      });
    }
  };

  // @ts-ignore
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <Form.Group size="lg" controlId="cas">
          <Row>
            <Col>
              <Form.Label>Čas</Form.Label>
            </Col>
          </Row>
          <Row>
            <Col>
              <input
                type="time"
                name="cas"
                value={cas}
                onChange={(e: any) => setCas(e.target.value)}
              />
            </Col>
          </Row>
        </Form.Group>
        <Form.Group size="lg" controlId="kod_po">
          <Form.Label>Kód záznamu</Form.Label>
          <Form.Control
            as="select"
            value={kod_po}
            onChange={(e) => setKod_po(e.target.value)}
          >
            {Object.values(EnumKodPo).map((item) => (
              <option value={item} key={item}>
                {enumKodPo_Caption(item)}
              </option>
            ))}
          </Form.Control>
        </Form.Group>

        <Form.Group size="lg" controlId="druh">
          <Form.Label>Druh průchodu</Form.Label>
          <Form.Control
            as="select"
            value={druh}
            onChange={(e) => setDruh(e.target.value)}
          >
            {Object.values(EnumDruhPruchodu).map((item) => (
              <option value={item} key={item}>
                {enumDruhPruchodu_Caption(item)}
              </option>
            ))}
          </Form.Control>
        </Form.Group>

        <Form.Group size="lg" controlId="poznamka">
          <Form.Label>Poznámka</Form.Label>
          <Form.Control
            as="textarea"
            rows={3}
            value={poznamka}
            onChange={(e) => setPoznamka(e.target.value)}
          />
        </Form.Group>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => handleSubmit()}>Uložit</Button>
        <Button onClick={() => handleClose()}>Zrušit</Button>
      </DialogActions>
    </Dialog>
  );
};

import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Button } from "react-bootstrap";
import VyhodnoceniService from "../service/VyhodnoceniService";

interface IProps {
  icp: string | null;
  date: Date | null;
  open: boolean;
  handleClose: () => void;
}
class CalculateDialog extends Component<IProps> {
  handleOk = () => {
    VyhodnoceniService.generateVyhodnoceni(
      this.props.icp,
      this.props.date
    ).then((data) => {
      this.props.handleClose();
    });
  };

  render() {
    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          Automatické vyhodnocení
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            Opravdu chcete provést automatické vyhodnocení?
          </DialogContentText>
          <DialogContentText>
            Vešketé zadané hodnoty budou přemazány.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleOk}>Ano</Button>
          <Button onClick={this.props.handleClose}>Ne</Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default CalculateDialog;

import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Button } from "react-bootstrap";
import KartaService from "../service/KartaService";
import VyhodnoceniService from "../service/VyhodnoceniService";

interface IProps {
  id: string | null;
  type: string | null;
  open: boolean;
  handleClose: () => void;
}
class RemoveDialog extends Component<IProps> {
  handleRemove = () => {
    if (this.props.type === "karta") {
      KartaService.deleteKarta(this.props.id).then(() => {
        this.props.handleClose();
      });
    } else {
      VyhodnoceniService.deleteVyhodnoceni(this.props.id).then(() => {
        this.props.handleClose();
      });
    }
  };

  render() {
    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Odebrat záznam</DialogTitle>
        <DialogContent>
          <DialogContentText>Opravdu chcete odebrat záznam?</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleRemove}>Ano</Button>
          <Button onClick={this.props.handleClose}>Ne</Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default RemoveDialog;

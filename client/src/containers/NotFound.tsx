import React from "react";
import "./NotFound.css";
import { Row } from "react-bootstrap";

export default function NotFound() {
  return (
    <div className="NotFound">
      <Row>
        <h2>404 - Stránka nebyla nalezena!</h2>
      </Row>
    </div>
  );
}

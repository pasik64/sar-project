import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
// @ts-ignore
import { withRouter } from "react-router-dom";
import "./Login.css";
import AuthService from "../service/AuthService";

interface IProps {}
interface IState {
  login: string;
  password: string;
  message: string;
}
class Login extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      login: "",
      password: "",
      message: "",
    };
  }

  onChangeLogin = (value: string) => {
    this.setState({
      login: value,
    });
  };

  onChangePassword = (value: string) => {
    this.setState({
      password: value,
    });
  };

  validateForm = () => {
    return this.state.login.length > 0 && this.state.password.length > 0;
  };

  handleSubmit = (event: React.FormEvent) => {
    event.preventDefault();

    AuthService.login(this.state.login, this.state.password).then(
      () => {
        // @ts-ignore
        this.props.history.push("/");
        window.location.reload();
      },
      (error) => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        this.setState({
          message: resMessage,
        });
      }
    );
  };

  render() {
    return (
      <div className="Login">
        <Form onSubmit={this.handleSubmit}>
          {/*@ts-ignore*/}
          <Form.Group size="lg" controlId="login">
            <Form.Label>Login</Form.Label>
            <Form.Control
              autoFocus
              type="text"
              value={this.state.login}
              onChange={(e) => this.onChangeLogin(e.target.value)}
            />
          </Form.Group>
          {/*@ts-ignore*/}
          <Form.Group size="lg" controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              value={this.state.password}
              onChange={(e) => this.onChangePassword(e.target.value)}
            />
          </Form.Group>

          {this.state.message && (
            <div className="form-group">
              <div className="alert alert-danger" role="alert">
                {this.state.message}
              </div>
            </div>
          )}

          <Button
            className={"mt-3"}
            block
            size="lg"
            type="submit"
            disabled={!this.validateForm()}
          >
            Login
          </Button>
        </Form>
      </div>
    );
  }
}

export default withRouter(Login);

import React from "react";
import "./Home.css";
import { Button, Col, Row } from "react-bootstrap";
// @ts-ignore
import { useHistory } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendar } from "@fortawesome/free-solid-svg-icons";

type Props = React.InputHTMLAttributes<HTMLInputElement> & {
  logged: boolean;
};
export const Home: React.FC<Props> = ({ logged }) => {
  const history = useHistory();

  const routeChange = (path: string) => {
    history.push(path);
  };

  return (
    <div className="Home">
      <Row>
        {logged === true ? (
          <>
            <Col sm={6} className={"mx-auto"}>
              <Button
                size={"lg"}
                className={"cardLink"}
                onClick={() => routeChange("attendance")}
              >
                <FontAwesomeIcon icon={faCalendar} className={"mr-3"} />{" "}
                Docházka
              </Button>
            </Col>
          </>
        ) : (
          <h2>Pro pokračování do aplikace je nutné se přihlásit</h2>
        )}
      </Row>
    </div>
  );
};

import React, { useState, useEffect } from "react";
import Button from "react-bootstrap/Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faEdit,
  faTrash,
  faPlus,
  faCalculator,
} from "@fortawesome/free-solid-svg-icons";
import { Col, Row } from "react-bootstrap";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import { ZamestnanecOverview } from "../components/ZamestnanecOverview";
import { KalendarOverview } from "../components/KalendarOverview";
import VyhodnoceniService from "../service/VyhodnoceniService";
import KartaService from "../service/KartaService";
import { Table } from "../components/Table";
import { Column } from "react-table";
import { getCaption as getKodPo } from "../enums/EnumKodPo";
import { getCaption as getDruhP } from "../enums/EnumDruhPruchodu";
import { getCaption as getKodDoby } from "../enums/EnumVyhodnoceni";
import { KartaVO } from "../VO/KartaVO";
import { UserVO } from "../VO/UserVO";
import { VyhodnoceniVO } from "../VO/VyhodnoceniVO";
import { getTime } from "../utils/TimeUtils";
import RemoveDialog from "../dialogs/RemoveDialog";
import CalculateDialog from "../dialogs/CalculateDialog";
import { AddKartaDialog } from "../dialogs/AddKartaDialog";
import { AddVyhodnoceniDialog } from "../dialogs/AddVyhodnoceniDialog";
import { KartaOverview } from "../components/KartaOverview";

type Props = React.InputHTMLAttributes<HTMLInputElement> & {
  logged: UserVO;
};
export const Attendance: React.FC<Props> = ({ logged }) => {
  const [karta, setKarta] = useState<Array<KartaVO>>([]);
  const [vyhodnoceni, setVyhodnoceni] = useState([]);
  const [date, setDate] = useState<Date>(new Date());

  const [removeId, setRemoveId] = useState<string | null>(null);
  const [displayCalc, setDisplayCalc] = useState<boolean>(false);
  const [removalType, setRemovalType] = useState<string>("karta");
  const [updateKarta, setUpdateKarta] = useState<KartaVO | null>(null);
  const [updateVyhodnoceni, setUpdateVyhodnoceni] =
    useState<VyhodnoceniVO | null>(null);
  const [kartaDialogTitle, setKartaDialogTitle] = useState<string | null>(null);
  const [vyhodnoceniDialogTitle, setVyhodnoceniDialogTitle] = useState<
    string | null
  >(null);

  const fetchData = () => {
    KartaService.getAll(date, logged.name).then((resp) => {
      setKarta(resp.data);
    });

    VyhodnoceniService.findVyhodnoceniByIcpAndDatum(logged.name, date).then(
      (resp) => {
        setVyhodnoceni(resp.data);
      }
    );
  };

  useEffect(
    () => {
      fetchData();
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [date, logged.name]
  );

  const columnsKarta: Column<KartaVO>[] = React.useMemo(
    () => [
      {
        Header: "Čas",
        accessor: "cas",

        Cell: (row) => <>{getTime(row.row.values.cas)}</>,
      },
      {
        Header: "Kód",
        accessor: "kod_po",
        Cell: (row) => <>{getKodPo(row.row.values.kod_po)}</>,
      },
      {
        Header: "Druh průchodu",
        accessor: "druh",
        Cell: (row) => <>{getDruhP(row.row.values.druh)}</>,
      },
      {
        Header: "Poznámka",
        accessor: "poznamka",
      },
      {
        Header: (
          <Button
            block
            size="sm"
            onClick={() => {
              setKartaDialogTitle("Přidat záznam");
            }}
          >
            <FontAwesomeIcon icon={faPlus} />
          </Button>
        ),
        accessor: "id",
        Cell: (row) => (
          <div className={"actions text-right"}>
            <Button
              className={"mr-1"}
              block
              size="sm"
              onClick={() => {
                setUpdateKarta(row.row.values);
                setKartaDialogTitle("Upravit záznam");
              }}
            >
              <FontAwesomeIcon icon={faEdit} />
            </Button>
            <Button
              block
              size="sm"
              onClick={() => {
                setRemovalType("karta");
                setRemoveId(row.row.values.id);
              }}
            >
              <FontAwesomeIcon icon={faTrash} />
            </Button>
          </div>
        ),
      },
    ],
    []
  );

  const columnsVyhodnoceni: Column<VyhodnoceniVO>[] = React.useMemo(
    () => [
      {
        Header: "Kód",
        accessor: "kod_doby",
        Cell: (row) => <>{getKodDoby(row.row.values.kod_doby)}</>,
      },
      {
        Header: "Počet hodin",
        accessor: "poc_hod",
      },
      {
        Header: (
          <div className={"actions text-right"}>
            <Button
              className={"mr-1"}
              block
              size="sm"
              onClick={() => {
                setDisplayCalc(true);
              }}
            >
              <FontAwesomeIcon icon={faCalculator} />
            </Button>
            <Button
              block
              size="sm"
              onClick={() => {
                setVyhodnoceniDialogTitle("Přidat záznam");
              }}
            >
              <FontAwesomeIcon icon={faPlus} />
            </Button>
          </div>
        ),
        // @ts-ignore
        accessor: "id",
        // @ts-ignore
        Cell: (row) => (
          <div className={"actions text-right"}>
            <Button
              className={"mr-1"}
              block
              size="sm"
              onClick={() => {
                setUpdateVyhodnoceni(row.row.values);
                setVyhodnoceniDialogTitle("Upravit záznam");
              }}
            >
              <FontAwesomeIcon icon={faEdit} />
            </Button>
            <Button
              block
              size="sm"
              onClick={() => {
                setRemovalType("vyhodnoceni");
                setRemoveId(row.row.values.id);
              }}
            >
              <FontAwesomeIcon icon={faTrash} />
            </Button>
          </div>
        ),
      },
    ],
    []
  );

  // @ts-ignore
  return (
    <div className="Attendence">
      <Row className={"mx-5"}>
        <Col sm={3}>
          <Calendar
            className={"mx-auto"}
            onChange={(selected: Date) => {
              //this is a fix necessary to use with calendar, there seems to be error in the library itself which results in wrong datum to be send to api
              setDate(
                new Date(
                  selected.getTime() - selected.getTimezoneOffset() * 60000
                )
              );
            }}
            value={date}
          />
        </Col>
        <Col sm={9}>
          <ZamestnanecOverview icp={logged.name} />
          <hr />
          <KartaOverview icp={logged.name} date={date} karty={karta} />
          <hr />
          <KalendarOverview datum={date} />
          <hr />
          <Table columns={columnsKarta} data={karta} />
          <hr />
          <Table columns={columnsVyhodnoceni} data={vyhodnoceni} />
        </Col>
      </Row>

      {/*DIALOGS OF THIS PAGE GO HERE*/}
      <RemoveDialog
        type={removalType}
        id={removeId}
        open={removeId !== null}
        handleClose={() => {
          setRemoveId(null);
          fetchData();
        }}
      />
      <CalculateDialog
        date={date}
        icp={logged.name}
        open={displayCalc === true}
        handleClose={() => {
          setDisplayCalc(false);
          fetchData();
        }}
      />
      <AddKartaDialog
        title={kartaDialogTitle}
        open={kartaDialogTitle !== null}
        initialValues={updateKarta}
        date={date}
        icp={logged.name}
        handleClose={() => {
          setUpdateKarta(null);
          setKartaDialogTitle(null);
          fetchData();
        }}
      />
      <AddVyhodnoceniDialog
        title={vyhodnoceniDialogTitle}
        open={vyhodnoceniDialogTitle !== null}
        initialValues={updateVyhodnoceni}
        date={date}
        icp={logged.name}
        handleClose={() => {
          setUpdateVyhodnoceni(null);
          setVyhodnoceniDialogTitle(null);
          fetchData();
        }}
      />
    </div>
  );
};
